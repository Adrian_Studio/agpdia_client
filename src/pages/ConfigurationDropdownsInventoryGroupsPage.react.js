// @flow

import React from "react";
import { Container, Row, Col, Card, Button, ModalTitle } from "react-bootstrap";

import GroupsList from '../components/configuration/GroupsList'
import ConfigurationMenu from '../components/configuration/ConfigurationMenu'
import SiteWrapper from "../SiteWrapper.react";

function Groups() {
  return (
    <SiteWrapper>
      <Container>
        <Row>
          <Col md={4} lg={3}>
            <ConfigurationMenu/>
          </Col>
          <Col md={8} lg={9}>
            <GroupsList/>
          </Col>
        </Row>
      </Container>
    </SiteWrapper>
  );
}

export default Groups;
