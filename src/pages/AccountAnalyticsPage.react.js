// @flow

import React from "react";
import moment from "moment";
import Infinite from "react-infinite";

import SiteWrapper from "../SiteWrapper.react";
import AccountFilter from "../components/accountGeneral/AccountFilter";
import DatePickerRow from "../components/DatePickerRow";
import YearPickerRow from "../components/accountAnalysis/montlySummary/YearPickerRow";
import YearRangePickerRow from "../components/accountAnalysis/yearlySummary/YearRangePickerRow";
import ModalForm from "../components/ModalForm";
import { Container, Row, Col, Card, Button, ModalTitle } from "react-bootstrap";
//import { Card } from "tabler-react";
import processAccountRecordsList from "../helpers/processAccountRecordsList";
import fetchFunction from "../helpers/fetchFuction";

import AccountRecord from "../components/accountRecords/AccountRecord";
import AccountRecordDate from "../components/accountRecords/AccountRecordDate";
import AccountRecordTotal from "../components/accountRecords/AccountRecordTotal";
import AccountAnalysisMonthly from "../components/accountAnalysis/montlySummary/AccountAnalysisMonthly";
import AccountAnalysisYearly from "../components/accountAnalysis/yearlySummary/AccountAnalysisYearly";
import AccountAnalysisGraphs from "../components/accountAnalysis/graphs/AccountAnalysisGraphs";
import AccountAnalysisInvoiceGraph from "../components/accountAnalysis/graphs/AccountAnalysisInvoiceGraph";
import AccountAnalysisVehicleCards from "../components/accountAnalysis/graphs/AccountAnalysisVehicleCards";
import AccountDetailsCard from "../components/accountAnalysis/AccountAnalysisCard";
import AccountRecordsList from "../components/accountRecords/AccountRecordsList";

import backButtonWithModal from "../helpers/backButtonWithModal";

function AccountRecords() {
  const [error, setError] = React.useState({});
  const [dateFilter, setDateFilter] = React.useState({});
  const [yearFilter, setYearFilter] = React.useState(moment().year());
  const [yearRangeFilter, setYearRangeFilter] = React.useState([
    moment().year(),
    moment().year(),
  ]);
  const [conceptsFilter, setConceptsFilter] = React.useState();
  const [accountsFilter, setAccountsFilter] = React.useState();
  const [typesFilter, setTypesFilter] = React.useState();
  const [noteFilter, setNoteFilter] = React.useState();
  const [groupsFilter, setGroupsFilter] = React.useState();
  const [subgroupsFilter, setSubgroupsFilter] = React.useState();
  const [vehiclesFilter, setVehiclesFilter] = React.useState();
  const [housesFilter, setHousesFilter] = React.useState();

  const [section, setSection] = React.useState("monthly");
  const [toShow, setToShow] = React.useState([]);
  const [update, setUpdate] = React.useState(false);

  var modalRef = React.useRef();

  const updateSignal = () => {
    setUpdate(!update);
  };

  const dateHandler = (startDate, endDate) => {
    setDateFilter({
      start: new Date(startDate).setHours(0, 0, 0, 0),
      end: new Date(endDate).setHours(23, 59, 59, 999),
    });
  };

  const yearHandler = (year) => {
    setYearFilter(year);
  };

  const yearRangeHandler = (year) => {
    setYearRangeFilter(year);
  };

  const recordsFilterHandler = (
    concepts,
    accounts,
    types,
    note,
    groups,
    subgroups,
    vehicles,
    houses
  ) => {
    setConceptsFilter(concepts);
    setAccountsFilter(accounts);
    setTypesFilter(types);
    setNoteFilter(note);
    setGroupsFilter(groups);
    setSubgroupsFilter(subgroups);
    setVehiclesFilter(vehicles);
    setHousesFilter(houses);
  };

  const invoiceFilterHandler = (subgroups) => {
    setSubgroupsFilter(subgroups);
  };

  const changeSection = (section) => {
    setSection(section);
  };

  React.useEffect(() => {
    switch (section) {
      case "monthly":
      case "yearly":
      case "graphs":
        setToShow(["groups", "accounts", "concepts", "subgroups", "types"]);
        break;
      case "house":
        setToShow([
          "accounts",
          "types",
          "houses",
          "house_concepts",
          "house_subgroups",
        ]);
        break;
      case "vehicle":
        setToShow([
          "accounts",
          "vehicle_subgroups",
          "types",
          "vehicles",
          "vehicle_concepts",
        ]);
        break;
      case "invoice":
        setToShow(["house", "types", "invoice_concepts"]);
        break;
      case "paysheet":
        setToShow(["types", "paysheet_concepts"]);
        break;
      case "stock":
        setToShow(["types", "stock_concepts", "stock"]);
        break;
      default:
        setToShow([]);
        break;
    }
  }, [section]);

  return (
    <SiteWrapper>
      <Container>
        <br></br>
        <Row>
          <Col lg={3}>
            <Card>
              <Card.Body
                style={{
                  display: "flex",
                  padding: "10px",
                }}
              >
                {section == "monthly" && (
                  <YearPickerRow parentCallBack={yearHandler} />
                )}
                {section == "yearly" && (
                  <YearRangePickerRow parentCallBack={yearRangeHandler} />
                )}
                {[
                  "graphs",
                  "house",
                  "vehicle",
                  "invoice",
                  "paysheet",
                  "stock",
                ].includes(section) && (
                  <DatePickerRow parentCallBack={dateHandler} initialFilterDate={true} />
                )}
              </Card.Body>
            </Card>
            <AccountFilter
              toShow={toShow}
              parentCallBack={recordsFilterHandler}
            />
          </Col>
          <Col lg={9} className="detailsTable">
            <AccountDetailsCard
              props={{
                section: section,
              }}
              changeSection={changeSection}
            />
            {["vehicle"].includes(
              section
            ) &&
            <AccountAnalysisVehicleCards
              props={{
                filter: {
                  start_date: dateFilter.start,
                  end_date: dateFilter.end,
                  concepts: conceptsFilter,
                  accounts: accountsFilter,
                  types: typesFilter,
                  note: noteFilter,
                  groups: ['vehicle'],
                  subgroups: subgroupsFilter,
                  vehicles: vehiclesFilter,
                },
              }}
            />}
            {section == "monthly" && (
              <AccountAnalysisMonthly
                props={{
                  hasDateFilter: true,
                  filter: {
                    year: yearFilter,
                    concepts: conceptsFilter,
                    accounts: accountsFilter,
                    types: typesFilter,
                    note: noteFilter,
                    groups: groupsFilter,
                    subgroups: subgroupsFilter,
                  },
                }}
              />
            )}
            {section == "yearly" && (
              <AccountAnalysisYearly
                props={{
                  hasDateFilter: true,
                  filter: {
                    year: yearRangeFilter,
                    concepts: conceptsFilter,
                    accounts: accountsFilter,
                    types: typesFilter,
                    note: noteFilter,
                    groups: groupsFilter,
                    subgroups: subgroupsFilter,
                  },
                }}
              />
            )}
            {["graphs", "house", "vehicle", "stock", "paysheet"].includes(
              section
            ) && (
              <AccountAnalysisGraphs
                props={{
                  hasDateFilter: true,
                  update: update,
                  filter: {
                    start_date: dateFilter.start,
                    end_date: dateFilter.end,
                    concepts: toShow.some((r) =>
                      [
                        "concepts",
                        "house_concepts",
                        "vehicle_concepts",
                        "invoice_concepts",
                        "stock_concepts",
                        "paysheet_concepts",
                      ].includes(r)
                    )
                      ? conceptsFilter
                      : undefined,
                    accounts: toShow.includes("accounts")
                      ? accountsFilter
                      : undefined,
                    types: toShow.includes("types") ? typesFilter : undefined,
                    note: toShow.includes("note") ? noteFilter : undefined,
                    groups: toShow.includes("groups")
                      ? groupsFilter
                      : toShow.includes("houses")
                      ? !groupsFilter
                        ? ["house"]
                        : groupsFilter
                      : toShow.includes("vehicles")
                      ? !groupsFilter
                        ? ["vehicle"]
                        : groupsFilter
                      : toShow.includes("stock_concepts")
                      ? !groupsFilter
                        ? ["stock"]
                        : groupsFilter
                      : undefined,
                    subgroups: toShow.some((r) =>
                      ["subgroups", "houses", "vehicles"].includes(r)
                    )
                      ? subgroupsFilter
                      : toShow.includes("paysheet_concepts")
                      ? !subgroupsFilter
                        ? ["paysheet"]
                        : subgroupsFilter
                      : toShow.includes("invoice_concepts")
                      ? !subgroupsFilter
                        ? ["water_house", "light_house", "gas_house"]
                        : subgroupsFilter
                      : undefined,
                    vehicles: toShow.includes("vehicles")
                      ? vehiclesFilter
                      : undefined,
                    houses: toShow.includes("houses")
                      ? housesFilter
                      : undefined,
                    stock_graph: ["stock"].includes(section) ? true : false,
                  },
                }}
              />
            )}
            {/* Same filters for AccountAnalysisInvoiceGraph and AccountAnalysisGraphs, but is not needed for the invoice graph*/}
            {["invoice"].includes(section) && (
              <AccountAnalysisInvoiceGraph
                parentCallBack={invoiceFilterHandler}
                props={{
                  hasDateFilter: true,
                  update: update,
                  filter: {
                    start_date: dateFilter.start,
                    end_date: dateFilter.end,
                    concepts: toShow.some((r) =>
                      [
                        "concepts",
                        "house_concepts",
                        "vehicle_concepts",
                        "invoice_concepts",
                        "stock_concepts",
                        "paysheet_concepts",
                      ].includes(r)
                    )
                      ? conceptsFilter
                      : undefined,
                    accounts: toShow.includes("accounts")
                      ? accountsFilter
                      : undefined,
                    types: toShow.includes("types") ? typesFilter : undefined,
                    note: toShow.includes("note") ? noteFilter : undefined,
                    groups: toShow.includes("groups")
                      ? groupsFilter
                      : toShow.includes("houses")
                      ? !groupsFilter
                        ? ["house"]
                        : groupsFilter
                      : toShow.includes("vehicles")
                      ? !groupsFilter
                        ? ["vehicle"]
                        : groupsFilter
                      : toShow.includes("stock_concepts")
                      ? !groupsFilter
                        ? ["stock"]
                        : groupsFilter
                      : undefined,
                    subgroups: toShow.includes("invoice_concepts")
                      ? !subgroupsFilter
                        ? ["light_house"]
                        : subgroupsFilter
                      : undefined,
                    // vehicles: toShow.includes("vehicles")
                    //   ? vehiclesFilter
                    //   : undefined,
                    houses: toShow.includes("houses")
                      ? housesFilter
                      : undefined,
                    // stock_graph: ["stock"].includes(section) ? true : false,
                  },
                }}
              />
            )}
            <br></br>
            {[
              "monthly",
              "yearly",
              "graphs",
              "house",
              "vehicle",
              "stock",
              "invoice",
              "paysheet",
            ].includes(section) && (
              <AccountRecordsList
                props={{
                  hasDateFilter: true,
                  filter: {
                    year:
                      section == "monthly"
                        ? yearFilter
                        : section == "yearly"
                        ? yearRangeFilter
                        : undefined,
                    start_date: dateFilter.start,
                    end_date: dateFilter.end,
                    concepts: toShow.some((r) =>
                      [
                        "concepts",
                        "house_concepts",
                        "vehicle_concepts",
                        "invoice_concepts",
                        "stock_concepts",
                        "paysheet_concepts",
                      ].includes(r)
                    )
                      ? conceptsFilter
                      : undefined,
                    accounts: toShow.includes("accounts")
                      ? accountsFilter
                      : undefined,
                    types: toShow.includes("types") ? typesFilter : undefined,
                    note: toShow.includes("note") ? noteFilter : undefined,
                    groups: toShow.includes("groups")
                      ? groupsFilter
                      : toShow.includes("houses")
                      ? !groupsFilter
                        ? ["house"]
                        : groupsFilter
                      : toShow.includes("vehicles")
                      ? !groupsFilter
                        ? ["vehicle"]
                        : groupsFilter
                      : toShow.includes("stock_concepts")
                      ? !groupsFilter
                        ? ["stock"]
                        : groupsFilter
                      : undefined,
                    subgroups: toShow.some((r) =>
                      ["subgroups", "houses", "vehicles"].includes(r)
                    )
                      ? subgroupsFilter
                      : toShow.includes("paysheet_concepts")
                      ? !subgroupsFilter
                        ? ["paysheet"]
                        : subgroupsFilter
                      : toShow.includes("invoice_concepts")
                      ? !subgroupsFilter
                        ? ["water_house", "light_house", "gas_house"]
                        : subgroupsFilter
                      : undefined,
                    vehicles: toShow.includes("vehicles")
                      ? vehiclesFilter
                      : undefined,
                    houses: toShow.includes("houses")
                      ? housesFilter
                      : undefined,
                  },
                  toShow: toShow,
                }}
                updateSignal={updateSignal}
              />
            )}
          </Col>
          <br></br>
        </Row>
      </Container>
    </SiteWrapper>
  );
}

export default AccountRecords;
