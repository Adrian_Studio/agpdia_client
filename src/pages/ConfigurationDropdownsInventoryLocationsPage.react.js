// @flow

import React from "react";
import { Container, Row, Col, Card, Button, ModalTitle } from "react-bootstrap";

import LocationsList from '../components/configuration/LocationsList'
import ConfigurationMenu from '../components/configuration/ConfigurationMenu'
import SiteWrapper from "../SiteWrapper.react";

function Locations() {
  return (
    <SiteWrapper>
      <Container>
        <Row>
          <Col md={4} lg={3}>
            <ConfigurationMenu/>
          </Col>
          <Col md={8} lg={9}>
            <LocationsList/>
          </Col>
        </Row>
      </Container>
    </SiteWrapper>
  );
}

export default Locations;
