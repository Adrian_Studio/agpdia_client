// @flow

import React from "react";
import {
  Page,
  Avatar,
  Icon,
  Grid,
  Card,
  Text,
  Table,
  Alert,
  Progress,
  colors,
  Dropdown,
  Button,
  StampCard,
  StatsCard,
  ProgressCard,
  Badge,
} from "tabler-react";
import { Container, Row, Col, ModalTitle } from "react-bootstrap";

import C3Chart from "react-c3js";

import SiteWrapper from "../SiteWrapper.react";

import StatsCardAccount from "../components/dashboard/StatsCardAccountDashboard";
import AccountRecordsListDashboard from "../components/dashboard/AccountRecordsListDashboard";
import InventoryItemsListDashboard from "../components/dashboard/InventoryItemsListDashboard";
import StatsCardAccountListDashboard from "../components/dashboard/StatsCardAccountListDashboard";
import TravelMapDashboard from "../components/dashboard/TravelMapDashboard";

function Dashboard() {
  return (
    <SiteWrapper>
      <Container>
        <br></br>
        <StatsCardAccountListDashboard />
        <br></br>
        <Row>
          <Col md={6}>
            <AccountRecordsListDashboard />
          </Col>
          <Col md={6}>
            <InventoryItemsListDashboard />
          </Col>
        </Row>
        <br></br>
        {window.innerWidth > 500 && <TravelMapDashboard />}
      </Container>
    </SiteWrapper>
  );
}

export default Dashboard;
