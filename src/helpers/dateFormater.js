const dateFormater = (date, purpose) => {
  if (!date) return
  switch (purpose) {
    case "show":
      var f = new Date(date);
      return f.getDate().toString().replace(/^(\d)$/, '0$1') + "/" + (f.getMonth() + 1).toString().replace(/^(\d)$/, '0$1') + "/" + f.getFullYear();
      break;

    case "database":
      return date.replace(/^(\d{1,2})\/(\d{1,2})\/(\d{4})$/, "$2/$1/$3")
      break;

    default:
      break;
  }
};

export default dateFormater;
