const currencyFormater = (amount, format, symbol, is_abs) => {

  if (amount == undefined) return
  var isNegative = false;
  if (amount < 0) {
    isNegative = true;
    amount = -amount;
  }
  var returnedAmount = null;
  if (format === "#.###,## ¤" || format === "¤ #.###,##") {
    returnedAmount = amount
      .toFixed(2)
      .replace(".", ",")
      .replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.");
  } else if (format === "#,###.## ¤" || format === "¤ #,###.##") {
    returnedAmount = amount.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, "$&,");
  } else {
    returnedAmount = amount;
  }
  if (format === "#.###,## ¤" || format === "#,###.## ¤") {
    returnedAmount = returnedAmount + symbol;
  } else if (format === "¤ #.###,##" || format === "¤ #,###.##") {
    returnedAmount = symbol + returnedAmount;
  } else {
    //returnedAmount = returnedAmount;
  }
  if (isNegative && !is_abs) {
    returnedAmount = "-" + returnedAmount;
  }
  return returnedAmount;
};

export default currencyFormater;
