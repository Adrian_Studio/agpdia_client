// @flow

import React from "react";
import { Redirect, useParams } from "react-router-dom";

import Infinite from "react-infinite";

import ModalForm from "../ModalForm";
import { Container, Row, Col, Card, Button, ModalTitle } from "react-bootstrap";

//import { Card } from "tabler-react";
import fetchFunction from "../../helpers/fetchFuction";

import InventoryItemDashboard from "../dashboard/InventoryItemDashboard";
import InventoryItemTotal from "../inventoryItems/InventoryItemTotal";
import InventoryItemForm from "../inventoryItems/InventoryItemForm";
import { useHistory } from "react-router-dom";

import backButtonWithModal from "../../helpers/backButtonWithModal";

function InventoryItemsList() {
  const history = useHistory();
  const [error, setError] = React.useState();
  const [modalTitle, setModalTitle] = React.useState("");
  const [items, setItems] = React.useState(false);
  const [itemsData, setItemsData] = React.useState();
  const [locations, setLocations] = React.useState();
  const [locationsInfo, setLocationsInfo] = React.useState();
  const [groups, setGroups] = React.useState();
  const [currencies, setCurrencies] = React.useState();
  // const [selectedItems, setSelectedItem] = React.useState([]);
  const [itemsToForm, setItemsToForm] = React.useState([]);
  const [allItems, setAllItems] = React.useState([]);
  const [prevProps, setPrevProps] = React.useState({});
  const [firstRender, setFirstRender] = React.useState(false);
  const [modalType, setModalType] = React.useState();

  var modalRef = React.useRef();

  React.useEffect(() => {
    fetchItems();
  }, []);

  const fetchItems = () => {
    var filter = { limit: 10 };
    setItems(false);
    var [promise, abort] = fetchFunction(
      "inventories/aggs/items/list",
      "POST",
      filter
    );
    promise.then((data) => {
      if (!data) {
        setItems([]);
      } else {
        setItemsData(data.items);
      }
    });
  };

  const makeItemsArray = () => {
    var itemsTable = [];

    // itemsTable.push(
    //   <InventoryItemTotal
    //     key={new Date().getTime()}
    //     props={{
    //       amount: itemsData.total,
    //     }}
    //     showModalHandler={showModalHandler}
    //   />
    // );
    itemsData.forEach((item) => {
      itemsTable.push(
        <InventoryItemDashboard
          key={item.id}
          props={item}
          // showModalHandler={showModalHandler}
          // refetchItems={refetchItems}
        />
      );
    });
    setItems(itemsTable);
  };

  const handleClick = (e) => {
    if (window.innerWidth > 500) {
      history.push("/inventory/dashboard");
    }
  };

  React.useEffect(() => {
    if (!itemsData) return; //Dont create the array if there is no data

    makeItemsArray();
  }, [itemsData]);

  return (
    <>
      <Card onClick={handleClick}>
        <Card.Body
          style={{
            padding: "0px 10px",
            background: "#f5f7fb",
            maxHeight: "500px",
            overflow: "auto",
          }}
        >
          {error && (
            <div className={"alert alert-" + error.type} role="alert">
              {error.message}
            </div>
          )}
          <Col lg={12} className="p-0">
            {!items ? (
              <>
                <br></br>
                <h2>Loading...</h2>
              </>
            ) : items.length ? (
              items
            ) : (
              <>
                <br></br>
                <h2 style={{ textAlign: "center" }}>Sin items el inventario</h2>
              </>
            )}
          </Col>{" "}
        </Card.Body>
      </Card>
    </>
  );
}

export default InventoryItemsList;
