// @flow

import React, { useEffect } from "react";
import fetchFunction from "../../helpers/fetchFuction";
import { Col, Row, Card } from "react-bootstrap";
import { Doughnut, Line, Bar } from "react-chartjs-2";
import dateLanguage from "../../helpers/dateLanguage";
import { Form, Button } from "tabler-react";
import Select from "react-select";
import moment from "moment";
import { VectorMap } from "react-jvectormap";

function TravelMap({ props, updateSignal }) {
  const [mapType, setMapType] = React.useState("country");
  const [records, setRecords] = React.useState([]);
  const [mapData, setMapData] = React.useState([]);
  const [markers, setMarkers] = React.useState([]);

  const fetchTravels = (continent, country, note) => {
    var filter = {};

    var [promise, abort] = fetchFunction(
      "travels/aggs/travels/list",
      "POST",
      filter
    );
    promise.then((data) => {
      if (!data) {
        setRecords([]);
      } else {
        setRecords(data);
        var markers = [];
        var mapData = {};
        data.map((record) => {
          if (!record.is_scheduled) {
            if (!mapData[record.countryCode]) {
              mapData[record.countryCode] = 0;
            }
            mapData[record.countryCode] += 1;

            if (
              record.hasOwnProperty("latitude") &&
              record.hasOwnProperty("longitude")
            ) {
              markers.push({
                latLng: [record.latitude, record.longitude],
                name: record.place,
              });
            }
          }
        });
        setMarkers(markers);
        setMapData(mapData);
      }
    });
  };
  var markerStyle = {
    initial: {
      fill: "#F8E23B",
      stroke: "#383f47",
    },
  };
  React.useEffect(() => {
    fetchTravels();
  }, []);
  const regionStyle = {
    initial: {
      fill: "white",
      "fill-opacity": 0.9,
      stroke: "none",
      "stroke-width": 0,
      "stroke-opacity": 0,
    },
  };

  const series = {
    regions: [
      {
        values: mapData,
        scale: ["#AAAAAA", "#444444"],
        normalizeFunction: "polynomial",
      },
    ],
  };
  const containerStyle = {
    width: "100%",
    height: "420px",
  };

  return (
    <Row onClick="console.log('¿?')">
      <Col md={12}>
        <VectorMap
          map={"world_mill"}
          backgroundColor="#C8E5EB"
          zoomOnScroll={true}
          containerStyle={containerStyle}
          containerClassName="jvectormap"
          regionStyle={regionStyle}
          series={series}
          markers={markers}
          markerStyle={markerStyle}
        />
      </Col>
    </Row>
  );
}

export default TravelMap;
