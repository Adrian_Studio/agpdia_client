import React, { useState, useRef } from "react";

import moment from "moment";
import DateRangePicker from "react-bootstrap-daterangepicker";
import textsCalendar from "../texts/Calendar";
import "bootstrap-daterangepicker/daterangepicker.css";
import { Col, Row, Button } from "react-bootstrap";
//import { Button } from "tabler-react";

const DatePickerRow = ({ parentCallBack, initialFilterDate }) => {
  const language = sessionStorage.getItem("language");
  const [state, setState] = useState({
    // start: moment({ year: 1993, month: 8, day: 26 }),
    start: initialFilterDate ? moment().subtract(4, "years").add(1, "day") : moment(0),
    end: moment(),
  });
  const { start, end } = state;
  const keyRef = useRef(Date.now());

  const buttonStyle = {
    color: "black",
    borderRadius: "50%",
    border: "1px solid rgb(204, 204, 204)",
    background: "white",
  };

  const handleCallback = (start, end) => {
    setState({ start, end });
    parentCallBack(start, end);
  };

  const moveRange = (isBackwards) => {
    var startArray = moment(start).format("DD/MM/YYYY").split("/");
    var endArray = moment(end).add(1, "day").format("DD/MM/YYYY").split("/");
    var multiplier = isBackwards ? -1 : 1;
    if (startArray[0] == endArray[0]) {
      var startDay = parseInt(startArray[0]);
      var yearDiff =
        multiplier * (parseInt(endArray[2]) - parseInt(startArray[2]));

      var startYear = parseInt(startArray[2]) + yearDiff;
      var endYear = parseInt(endArray[2]) + yearDiff;

      var monthDiff =
        multiplier * (parseInt(endArray[1]) - parseInt(startArray[1]));

      var startMonth = parseInt(startArray[1]) + monthDiff;
      var endMonth = parseInt(endArray[1]) + monthDiff;

      if (isBackwards && startMonth == 23) {
        startMonth -= 24;
        startYear += 2;
        endYear -= 1;
      }

      if (isBackwards && endMonth == 12 && startArray[2] == endArray[2] - 1) {
        endYear += 1;
      }
      if (!isBackwards && endMonth == -10 && startArray[2] == endArray[2] - 1) {
        endYear -= 1;
      }
      if (startMonth > 12) {
        startMonth -= 12;
      } else if (startMonth <= 0) {
        startMonth += 12;
        startYear -= 1;
      }

      if (endMonth > 12) {
        endMonth -= 12;
        endYear += 1;
      } else if (endMonth <= 0) {
        endMonth += 12;
      }

      var tempStart = startMonth + "/" + startDay + "/" + startYear;
      var tempEnd = endMonth + "/" + startDay + "/" + endYear;

      var newStart = moment(new Date(tempStart));
      var newEnd = moment(new Date(tempEnd)).subtract(1, "days");
    } else {
      var difference = end - start + 86400000;
      difference = isBackwards ? -difference : difference;

      var newStart = moment(new Date(start + difference));
      var newEnd = moment(new Date(end + difference));
    }

    keyRef.current = Date.now();
    handleCallback(newStart, newEnd);
  };

  React.useEffect(() => {
    parentCallBack(start, end);
  }, []);

  // var label = start.format("DD/MM/YYYY") + " - " + end.format("DD/MM/YYYY");
  // if (start.format("DD/MM/YYYY") == end.format("DD/MM/YYYY")) {
  //   var label = start.format("DD/MM/YYYY");
  // }
  return (
    <Row style={{ margin: "auto" }}>
      <Button size="sm" style={buttonStyle} onClick={() => moveRange(true)}>
        {"<"}
      </Button>
      <DateRangePicker
        key={keyRef.current}
        initialSettings={{
          linkedCalendars: false,
          startDate: start,
          endDate: end,
          opens: "center",
          alwaysShowCalendars: true,
          locale: {
            format: "DD/MM/YYYY",
            firstDay: 1,
            cancelLabel: textsCalendar.cancelButton[language],
            applyLabel: textsCalendar.applyButton[language],
            todayLabel: "Hoy",
            customRangeLabel: textsCalendar.customRangeLabel[language],
            daysOfWeek: textsCalendar.daysOfWeek[language],
            monthNames: textsCalendar.monthNames[language],
          },
          ranges: {
            //   Today: [moment().toDate(), moment().toDate()],
            //   Yesterday: [
            //     moment().subtract(1, "days").toDate(),
            //     moment().subtract(1, "days").toDate(),
            //   ],
            // "Last 7d (Week)": [
            //   moment().subtract(6, "days").toDate(),
            //   moment().toDate(),
            // ],
            [textsCalendar.ranges.last30d[language]]: [
              moment().subtract(29, "days").toDate(),
              moment().toDate(),
            ],
            // "Last 365d (Year)": [
            //   moment().subtract(364, "days").toDate(),
            //   moment().toDate(),
            // ],
            [textsCalendar.ranges.thisWeek[language]]: [
              moment().startOf("week").toDate(),
              moment().endOf("week").toDate(),
            ],
            [textsCalendar.ranges.thisMonth[language]]: [
              moment().startOf("month").toDate(),
              moment().endOf("month").toDate(),
            ],
            [textsCalendar.ranges.thisYear[language]]: [
              moment().startOf("year").toDate(),
              moment().endOf("year").toDate(),
            ],
            [textsCalendar.ranges.untilToday[language]]: [
              moment(new Date('2015-01-01')).toDate(),
              moment().toDate(),
            ],
          },
        }}
        onCallback={handleCallback}
        onShow={(e) => {
          e.target.blur();
        }}
        onEvent={(e) => console.log(e)}
      >
        <input
          type="text"
          className="form-control"
          style={{ width: "180px", textAlign: "center", maxWidth: "200px" }}
        />
      </DateRangePicker>
      <Button size="sm" style={buttonStyle} onClick={() => moveRange(false)}>
        {">"}
      </Button>
    </Row>
  );
};

export default DatePickerRow;
