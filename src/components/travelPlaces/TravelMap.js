// @flow

import React, { useEffect } from "react";
import fetchFunction from "../../helpers/fetchFuction";
import { Col, Row, Card } from "react-bootstrap";
import { Doughnut, Line, Bar } from "react-chartjs-2";
import dateLanguage from "../../helpers/dateLanguage";
import { Form, Button } from "tabler-react";
import Select from "react-select";
import moment from "moment";
import { VectorMap } from "react-jvectormap";

function TravelMap({ props, updateSignal }) {
  const [mapType, setMapType] = React.useState("country");
  const [map, setMap] = React.useState("world_mill");

  var mapDataContinent = {};
  var mapDataCountry = {};
  var mapDataRegion = {};
  var visitedPlaces = 0;
  var visitedRegions = 0;
  var visitedCountries = 0;
  var visitedContinents = 0;
  var markers = [];
  props.records.map((record) => {
    if (!record.is_scheduled) {
      if (!mapDataCountry[record.countryCode]) {
        mapDataCountry[record.countryCode] = 0;
      }
      mapDataCountry[record.countryCode] += 1;
      if (!mapDataContinent[record.continentCode]) {
        mapDataContinent[record.continentCode] = 0;
      }
      mapDataContinent[record.continentCode] += 1;
      if (record.countryCode && record.regionCode) {
        if (!mapDataRegion[record.regionCode]) {
          mapDataRegion[`${record.countryCode}-${record.regionCode}`] = 0;
        }
        mapDataRegion[`${record.countryCode}-${record.regionCode}`] += 1;
      }
      if (
        record.hasOwnProperty("latitude") &&
        record.hasOwnProperty("longitude")
      ) {
        if (record.countryCode == "ES" || props.map != "spainMap") {
          markers.push({
            latLng: [record.latitude, record.longitude],
            name: record.place,
          });
        }
      }
    }
  });
  visitedContinents = Object.keys(mapDataContinent).length;
  visitedCountries = Object.keys(mapDataCountry).length;
  visitedRegions = Object.keys(mapDataRegion).length;
  visitedPlaces = Object.keys(props.records).length;

  var markerStyle = {
    initial: {
      fill: "#F8E23B",
      stroke: "#383f47",
    },
  };

  const regionStyle = {
    initial: {
      fill: "white",
      "fill-opacity": 0.9,
      stroke: "none",
      "stroke-width": 0,
      "stroke-opacity": 0,
    },
  };

  const series = {
    regions: [
      {
        values:
          mapType == "country"
            ? mapDataCountry
            : mapType == "region"
            ? mapDataRegion
            : mapType == "continent"
            ? mapDataContinent
            : undefined,
        scale: ["#AAAAAA", "#444444"],
        normalizeFunction: "polynomial",
      },
    ],
  };
  const containerStyle = {
    width: "100%",
    height: "420px",
  };
  React.useEffect(() => {
    var map = "worldMap";
    var mapType = "country";

    if (props.map == "worldMap") {
      map = "world_mill";
      mapType = "country";
    }
    if (props.map == "europeMap") {
      map = "europe_mill";
      mapType = "country";
    }
    if (props.map == "spainMap") {
      map = "es_mill";
      mapType = "region";
    }

    setMap(map);
    setMapType(mapType);
  }, [props]);
  return (
    <>
      <p style={{ textAlign: "center", fontWeight: "bold", marginTop: "1rem"}}>
        {!visitedContinents
          ? ""
          : map == "world_mill"
          ? `Se han visitado ${visitedPlaces} lugares de ${visitedCountries} paises en ${visitedContinents} continentes`
          : map == "europe_mill"
          ? `Se han visitado ${visitedPlaces} lugares de ${visitedCountries} paises`
          : map == "es_mill"
          ? `Se han visitado ${visitedPlaces} lugares en al menos ${visitedRegions} provincias`
          : ""}
      </p>
      {map == "world_mill" && (
        <VectorMap
          map={map}
          backgroundColor="#C8E5EB"
          zoomOnScroll={true}
          containerStyle={containerStyle}
          containerClassName="jvectormap"
          regionStyle={regionStyle}
          series={series}
          markers={markers}
          markerStyle={markerStyle}
        />
      )}
      {map == "europe_mill" && (
        <VectorMap
          map={map}
          backgroundColor="#C8E5EB"
          zoomOnScroll={true}
          containerStyle={containerStyle}
          containerClassName="jvectormap"
          regionStyle={regionStyle}
          series={series}
          markers={markers}
          markerStyle={markerStyle}
        />
      )}
      {map == "es_mill" && (
        <VectorMap
          map={map}
          backgroundColor="#C8E5EB"
          zoomOnScroll={true}
          containerStyle={containerStyle}
          containerClassName="jvectormap"
          regionStyle={regionStyle}
          series={series}
          markers={markers}
          markerStyle={markerStyle}
        />
      )}
    </>
  );
}

export default TravelMap;
