// @flow

import React from "react";
import { Redirect, useParams } from "react-router-dom";

import Infinite from "react-infinite";

import ModalForm from "../ModalForm";
import { Col, Row } from "react-bootstrap";

//import { Card } from "tabler-react";
import fetchFunction from "../../helpers/fetchFuction";

import InventoryItem from "../inventoryItems/InventoryItem";
import InventoryItemTotal from "../inventoryItems/InventoryItemTotal";
import InventoryItemForm from "../inventoryItems/InventoryItemForm";

import backButtonWithModal from "../../helpers/backButtonWithModal";

function InventoryItemsList({ props, updateSignal }) {
  const [error, setError] = React.useState();
  const [modalTitle, setModalTitle] = React.useState("");
  const [itemsTotal, setItemsTotal] = React.useState(false);
  const [items, setItems] = React.useState(false);
  const [itemsData, setItemsData] = React.useState();
  const [locations, setLocations] = React.useState();
  const [locationsInfo, setLocationsInfo] = React.useState();
  const [groups, setGroups] = React.useState();
  const [currencies, setCurrencies] = React.useState();
  // const [selectedItems, setSelectedItem] = React.useState([]);
  const [itemsToForm, setItemsToForm] = React.useState([]);
  const [allItems, setAllItems] = React.useState([]);
  const [prevProps, setPrevProps] = React.useState({});
  const [firstRender, setFirstRender] = React.useState(false);
  const [modalType, setModalType] = React.useState();
  const [lastFetchAbort, setLastFetchAbort] = React.useState(); //Para guardar el abort del fetch actual
  var modalRef = React.useRef();

  React.useEffect(() => {
    // if (!firstRender) return;
    if (JSON.stringify(props) == JSON.stringify(prevProps)) return;
    setPrevProps(props);
  }, [props]);

  React.useEffect(() => {
    if (!firstRender) return;
    if (JSON.stringify(prevProps) == JSON.stringify({})) return;
    refetchItems();
  }, [prevProps]);

  const fetchItems = () => {
    if (lastFetchAbort) {
      lastFetchAbort();
    }
    var filter = {
      locations: props.filter.locations,
      groups: props.filter.groups,
      state: props.filter.state,
      note: props.filter.note,
    };
    setItems(false);
    setItemsTotal(false);
    var [promise, abort] = fetchFunction(
      "inventories/aggs/items/list",
      "POST",
      filter
    );
    promise.then((data) => {
      if (data == undefined) return
      if (!data) {
        setItemsTotal(
          <InventoryItemTotal
            key={new Date().getTime()}
            props={{ currencies: currencies }}
            showModalHandler={showModalHandler}
          />
        );
        setItemsData([]);
      } else {
        setItemsData(data.items);
      }
    });
    setLastFetchAbort(() => abort);
  };

  const fetchGroups = () => {
    var [promise, abort] = fetchFunction("inventories/groups/", "GET");
    promise.then((data) => {
      if (!data) {
        // No action
      } else {
        setGroups(data);
      }
    });
  };

  const fetchLocations = () => {
    var [promise, abort] = fetchFunction(
      "inventories/aggs/locations/filter_list",
      "GET"
    );
    promise.then((data) => {
      if (!data) {
        // No action
      } else {
        setLocations(data);
      }
    });
  };

  const fetchLocationsInfo = () => {
    var [promise, abort] = fetchFunction("inventories/locations", "GET");
    promise.then((data) => {
      if (!data) {
        // No action
      } else {
        setLocationsInfo(data);
      }
    });
  };

  const fetchCurrencies = () => {
    var [promise, abort] = fetchFunction("currencies/", "GET");
    promise.then((data) => {
      if (!data) {
        // No action
      } else {
        setCurrencies(data);
      }
    });
  };

  const makeItemsArray = () => {
    var itemsTable = [];
    var total = 0;
    
    itemsData.forEach((item) => {
      total += item.acquisition_price
      itemsTable.push(
        <InventoryItem
          key={item.id}
          props={item}
          showModalHandler={showModalHandler}
          refetchItems={refetchItems}
        />
      );
    });
    setItemsTotal(
      <InventoryItemTotal
        key={new Date().getTime()}
        props={{
          amount: total,
          elements: itemsTable.length
        }}
        showModalHandler={showModalHandler}
      />
    );
    setItems(itemsTable);
  };
  const dateHandler = (startDate, endDate) => {};
  const itemsFilterHandler = (locations, groups, type, note) => {};

  // const itemsCheckBoxHandler = (objectId, isChecked, relatedObjectId) => {
  //   if (isChecked) {
  //     var temp = [...selectedItems]; // Need to clone the object to prevent malfunction
  //     temp.push(objectId);
  //     if (relatedObjectId) temp.push(relatedObjectId);
  //     setSelectedItem(temp);
  //   } else {
  //     var temp = [...selectedItems]; // Need to clone the object to prevent malfunction
  //     temp.splice(temp.indexOf(objectId), 1);
  //     if (relatedObjectId) temp.splice(temp.indexOf(relatedObjectId), 1);
  //     setSelectedItem(temp);
  //   }
  // };

  // const totalCheckBoxHandler = (isChecked) => {
  //   if (isChecked) {
  //     setSelectedItem(allItems);
  //   } else {
  //     setSelectedItem([]);
  //   }
  // };
  const showModalHandler = (objectId, toAdd) => {
    objectId ? setItemsToForm([objectId]) : setItemsToForm();
    if (toAdd) {
      if (objectId && objectId.length) {
        setModalTitle("Copiar elementos");
        setModalType("Copy");
      } else {
        setModalTitle("Añadir elementos");
        setModalType("Add");
      }
    } else {
      setModalTitle("Editar elemento");
      setModalType("Update");
    }
    backButtonWithModal.neutralizeBack(modalRef.current);
    modalRef.current.handleShow();
    modalRef.current.task(objectId, toAdd);
  };

  const modalSaveHandler = () => {
    modalRef.current.handleHide();

    if (updateSignal) {
      updateSignal();
    }
    refetchItems();
  };

  const refetchItems = () => {
    fetchItems();
  };

  React.useEffect(() => {
    if (!groups) fetchGroups();

    if (!locations) fetchLocations();

    if (!locationsInfo) fetchLocationsInfo();

    if (!currencies) fetchCurrencies();

    // if (props.filter.groups) refetchItems();
    setFirstRender(true);
  }, []);

  React.useEffect(() => {
    if (!itemsData) return; //Dont create the array if there is no data
    // if (!items && selectedItems.length) {
    //   setSelectedItem([]); //Thil will trigger again this useEffect
    //   return; //This cancel the creation of the array this time
    // }

    makeItemsArray();
  }, [itemsData]);

  return (
    <>
      {error && (
        <div className={"alert alert-" + error.type} role="alert">
          {error.message}
        </div>
      )}
      <Col lg={12}>
        {itemsTotal && <Row>{itemsTotal}</Row>}
        {!items ? (
          <h1>Loading...</h1>
        ) : (
          <>
            {/* <Row>
                <Infinite
                //I don't know why but looks that with 1000 works for 2 columns
                elementHeight={800} // The height of the tallest element in the list
                useWindowAsScrollContainer
                
              > */}
            <Row>{items}</Row>
            {/* </Infinite>
            </Row> */}
          </>
        )}
      </Col>
      <ModalForm
        ref={modalRef}
        props={{
          title: modalTitle,
          className: "itemsForm",
        }}
      >
        <InventoryItemForm
          props={{
            objectId: itemsToForm,
            locations: locations,
            locationsInfo: locationsInfo,
            groups: groups,
            modalType: modalType,
            currencies: currencies,
          }}
          modalSaveHandler={modalSaveHandler}
        />
      </ModalForm>
    </>
  );
}

export default InventoryItemsList;
