import React, { useEffect, useState, useRef } from "react";

import { Modal, Col, Row } from "react-bootstrap";
import DateRangePicker from "react-bootstrap-daterangepicker";
import { Form, Button } from "tabler-react";
import Select from "react-select";
import fetchFunction from "../../helpers/fetchFuction";
import resizeBase64Img from "../../helpers/resizeBase64Img";
import textsCalendar from "../../texts/Calendar";
import dateFormater from "../../helpers/dateFormater";

const InventoryItemForm = ({ props, modalSaveHandler }) => {
  const language = sessionStorage.getItem("language");
  const [currencySymbol, setCurrencySymbol] = useState("€");
  const [initialItem, setInitialItem] = useState(false);
  const [fetchedItem, setFetchedItem] = useState(false);
  const [newItem, setNewItem] = useState(false);
  const [validForm, setValidForm] = useState();

  const [currencyOptions, setCurrencyOptions] = useState();
  const [locationOptions, setLocationOptions] = useState();
  const [groupOptions, setGroupOptions] = useState();
  const [inventoryOptions, setInventoryOptions] = useState();
  const [allLocationOptions, setAllLocationOptions] = useState();

  const [imagesOptions, setImagesOptions] = useState([]);

  const [initialAmount, setInitialAmount] = useState();
  const [color, setColor] = useState("");
  const [currency, setCurrency] = useState();

  const keyRef = useRef(Date.now()); // Needed to update the date
  keyRef.current = Date.now(); // Needed to update the date

  const [name, setName] = useState();
  const [inventory, setInventory] = useState();
  const [location, setLocation] = useState();
  const [group, setGroup] = useState();
  const [brand, setBrand] = useState();
  const [identifier, setIdentifier] = useState();
  const [acquisitionPlace, setAcquisitionPlace] = useState();
  const [acquisitionDate, setAcquisitionDate] = useState();
  const [acquisitionPrice, setAcquisitionPrice] = useState();
  const [disposeDate, setDisposeDate] = useState();
  const [note, setNote] = useState();
  const [state, setState] = useState();
  const [image, setImage] = useState();
  const [invoice, setInvoice] = useState();

  const fetchItem = (objectId) => {
    var [promise, abort] = fetchFunction(
      "inventories/items/" + objectId,
      "GET"
    );
    promise.then((data) => {
      if (!data) {
        // No action
      } else {
        setFetchedItem(data);
        setFields(data);
      }
    });
  };

  const handleSave = () => {
    var processedNewItem = newItem;
    processedNewItem.acquisition_date = dateFormater(
      processedNewItem.acquisition_date,
      "database"
    );
    if (processedNewItem.dispose_date)
      processedNewItem.dispose_date = dateFormater(
        processedNewItem.dispose_date,
        "database"
      );

    switch (props.modalType) {
      case "Add":
      case "Copy":
        var [promise, abort] = fetchFunction(
          "inventories/items/",
          "POST",
          newItem
        );
        promise.then((data) => {
          if (!data) {
            // No action
          } else {
            modalSaveHandler();
          }
        });
        break;
      case "Update":
        var objectId = props.objectId;
        var [promise, abort] = fetchFunction(
          "inventories/items/" + objectId,
          "PUT",
          processedNewItem
        );
        promise.then((data) => {
          if (!data) {
            // No action
          } else {
            modalSaveHandler();
          }
        });
        break;

      default:
        break;
    }
  };

  const setFields = (data) => {
    if (data) {
      setName(data.name);
      setInventory({ label: data.inventory_name, value: data.inventory_id });
      setLocation({ label: data.location_name, value: data.location_id });
      setGroup({ label: data.group_name, value: data.group_id });
      setBrand(data.brand);
      setIdentifier(data.identifier);
      setAcquisitionPlace(data.acquisition_place);
      setAcquisitionDate(dateFormater(data.acquisition_date, "show"));
      setAcquisitionPrice(data.acquisition_price);
      setDisposeDate(dateFormater(data.dispose_date, "show"));
      setNote(data.note);
      setState(data.state);
      setImage(data.image);
      setInvoice(data.invoice);
      var backData = {
        name: data.name,
        currency_id: data.currency_id,
        inventory_id: data.inventory_id,
        location_id: data.location_id,
        group_id: data.group_id,
        acquisition_price:
          data.acquisition_price &&
          data.acquisition_price.toString().replace(",", "."),
        acquisition_date: dateFormater(data.acquisition_date, "show"),
        acquisition_place: data.acquisition_place,
        dispose_date: dateFormater(data.dispose_date, "show"),
        brand: data.brand,
        identifier: data.identifier,
        note: data.note,
        state: data.state,
        image: data.image,
        invoice: data.invoice,
      };
    } else {
      setName();
      setInventory();
      setLocation();
      setGroup();
      setBrand();
      setIdentifier();
      setAcquisitionPlace();
      setAcquisitionDate(dateFormater(new Date(), "show"));
      setAcquisitionPrice();
      setDisposeDate();
      setNote();
      setState();
      setImage();
      setInvoice();
      var backData = {
        name: undefined,
        currency_id: props.currencies ? props.currencies[0]._id : undefined,
        inventory_id: undefined,
        location_id: undefined,
        group_id: undefined,
        acquisition_price: undefined,
        acquisition_date: dateFormater(new Date(), "show"),
        acquisition_place: undefined,
        dispose_date: undefined,
        brand: undefined,
        identifier: undefined,
        note: undefined,
        state: undefined,
        image: undefined,
        invoice: undefined,
      };
    }
    setInitialItem(backData);
    setNewItem(backData);
  };

  React.useEffect(() => {
    if (props.currencies) {
      var tempCurrencies = [];
      props.currencies.forEach((currency) => {
        if (currency.is_main)
          setCurrency({
            label: currency.symbol + "→" + currency.code,
            value: currency._id,
          });
        tempCurrencies.push({
          label: currency.symbol + "→" + currency.code,
          value: currency._id,
        });
      });

      setCurrencyOptions(tempCurrencies);
    }
    if (props.inventories) {
      var tempInventories = [];
      props.inventories.forEach((inventory) => {
        tempInventories.push({
          label: inventory.name,
          value: inventory._id,
        });
      });

      setInventoryOptions(tempInventories);
    }

    if (props.locations) {
      var tempInventories = [];
      var tempLocations = {};
      props.locations.forEach((inventory) => {
        tempInventories.push({ label: inventory.name, value: inventory._id });
        tempLocations[inventory._id] = [];
        inventory.locations.forEach((location) => {
          tempLocations[inventory._id].push({
            label: location.name,
            value: location.id,
          });
        });
      });
      setInventoryOptions(tempInventories);
      setAllLocationOptions(tempLocations);
    }

    if (props.groups) {
      var tempGroups = [];
      props.groups.forEach((group) => {
        tempGroups.push({
          label: group.name,
          value: group._id,
        });
      });

      setGroupOptions(tempGroups);
    }

    if (props.modalType == "Update" || props.modalType == "Copy") {
      fetchItem(props.objectId);
    } else if (props.modalType == "Add") {
      setFields();
    }
  }, []);

  React.useEffect(() => {
    var backData = {
      name: name,
      currency_id: currency ? currency.value : undefined,
      inventory_id: inventory ? inventory.value : undefined,
      location_id: location ? location.value : undefined,
      group_id: group ? group.value : undefined,
      acquisition_price:
        acquisitionPrice && acquisitionPrice.toString().replace(",", "."),
      acquisition_date: acquisitionDate,
      acquisition_place: acquisitionPlace,
      dispose_date: disposeDate,
      brand: brand,
      identifier: identifier,
      note: note,
      state: state,
      image: image,
      invoice: invoice,
    };

    if (
      name &&
      /^(\-)?[0-9]{0,9}([\.\,]([0-9]{1,2})?)?$/i.test(acquisitionPrice) &&
      currency &&
      acquisitionDate &&
      inventory &&
      group &&
      location
    ) {
      setValidForm(true);
    } else {
      setValidForm(false);
    }

    setNewItem(backData);
  }, [
    name,
    inventory,
    location,
    group,
    brand,
    identifier,
    acquisitionPlace,
    acquisitionDate,
    acquisitionPrice,
    disposeDate,
    note,
    state,
    image,
    invoice,
  ]);

  const uploadImage = (e) => {
    let images = e.target.files;

    if (images.length) {
      let reader = new FileReader();
      reader.readAsDataURL(images[0]);
      reader.onload = (e) => {
        resizeBase64Img(e.target.result, 500, 500).then((image) => {
          setImage(image);
        });
      };
    } else {
      setImage();
    }
  };

  return (
    <>
      <Row style={{ margin: "0px" }} className="coloredForm">
        <Col md={12} style={{ padding: "0px" }}>
          <div
            style={{
              background: "rgb(144, 164, 174)",
              color: "white",
              padding: "1rem",
            }}
          >
            <Row>
              <Col md={4}>
                <Form.Group label="Inventorio *">
                  <div style={{ color: "rgb(73, 80, 87)", width: "100%" }}>
                    <Select
                      onChange={(e) => {
                        setInventory(e);
                        if (inventory != e) setLocation();
                      }}
                      isSearchable={false}
                      value={{
                        label: inventory ? inventory.label : "Sel. inventorio",
                      }}
                      options={inventoryOptions}
                    />
                  </div>
                </Form.Group>
              </Col>
              <Col md={4}>
                <Form.Group label="Ubicación *">
                  <div style={{ color: "rgb(73, 80, 87)", width: "100%" }}>
                    <Select
                      onChange={(e) => {
                        setLocation(e);
                      }}
                      isSearchable={false}
                      value={{
                        label: location ? location.label : "Sel. ubicación",
                      }}
                      options={
                        inventory
                          ? allLocationOptions[inventory.value]
                          : undefined
                      }
                    />
                  </div>
                </Form.Group>
              </Col>
              <Col md={4}>
                <Form.Group label="Grupo *">
                  <div style={{ color: "rgb(73, 80, 87)", width: "100%" }}>
                    <Select
                      onChange={(e) => {
                        setGroup(e);
                      }}
                      isSearchable={false}
                      value={{
                        label: group ? group.label : "Sel. grupo",
                      }}
                      options={groupOptions}
                    />
                  </div>
                </Form.Group>
              </Col>
              <Col md={6}>
                <Form.Group label="Nombre *" style={{ paddingRight: "10px" }}>
                  <Form.Input
                    name="name"
                    className="input"
                    placeholder="Nombre..."
                    autoComplete="off"
                    type="text"
                    value={name ? name : ""}
                    onChange={(e) => {
                      setName(e.target.value);
                    }}
                  />
                </Form.Group>
              </Col>
              <Col md={3}>
                <Form.Group
                  label="Importe *"
                  className="field-amount"
                  style={{ paddingRight: "10px" }}
                >
                  <Form.Input
                    name="acquisitionPrice"
                    className="input"
                    placeholder="0,00"
                    autoComplete="off"
                    type="text"
                    value={
                      acquisitionPrice != undefined
                        ? acquisitionPrice.toString().replace(".", ",")
                        : ""
                    }
                    onChange={(e) => {
                      var temp = e.target.value;

                      if (/^(\-)?[0-9]{0,9}([\.\,]([0-9]{1,2})?)?$/i.test(temp))
                        temp
                          ? setAcquisitionPrice(temp)
                          : setAcquisitionPrice();
                    }}
                  />
                </Form.Group>
              </Col>

              <Col md={3}>
                <Form.Group label="Moneda *">
                  <div style={{ color: "rgb(73, 80, 87)", width: "100%" }}>
                    <Select
                      onChange={(e) => {
                        setCurrency(e);
                      }}
                      isSearchable={false}
                      isDisabled
                      value={{
                        label: currency ? currency.label : "Selecciona moneda",
                      }}
                      options={currencyOptions}
                    />
                  </div>
                </Form.Group>
              </Col>
              <Col md={6}>
                <Form.Group
                  label="Lugar obtención"
                  style={{ paddingRight: "10px" }}
                >
                  <Form.Input
                    name="place"
                    className="input"
                    placeholder="Lugar..."
                    autoComplete="off"
                    type="text"
                    value={acquisitionPlace ? acquisitionPlace : ""}
                    onChange={(e) => {
                      if (e.target.value.length) {
                        setAcquisitionPlace(e.target.value);
                      } else {
                        setAcquisitionPlace();
                      }
                    }}
                  />
                </Form.Group>
              </Col>
              <Col md={6}>
                <Form.Group label="Fecha obtención *">
                  <DateRangePicker
                    key={keyRef.current} // Needed to update the date
                    initialSettings={{
                      singleDatePicker: true,
                      opens: "center",
                      startDate: acquisitionDate,
                      locale: {
                        format: "DD/MM/YYYY",
                        firstDay: 1,
                        cancelLabel: textsCalendar.cancelButton[language],
                        applyLabel: textsCalendar.applyButton[language],
                        todayLabel: "Hoy",
                        customRangeLabel:
                          textsCalendar.customRangeLabel[language],
                        daysOfWeek: textsCalendar.daysOfWeek[language],
                        monthNames: textsCalendar.monthNames[language],
                      },
                    }}
                    onCallback={(e) =>
                      setAcquisitionDate(dateFormater(e._d, "show"))
                    }
                    onShow={(e) => {
                      e.target.blur();
                      // if (!$(".daterangepicker .drp-buttons .todayBtn").length)
                      //   $(".daterangepicker .drp-buttons").prepend(
                      //     `<button class="todayBtn btn btn-sm btn-default" type="button" onClick="setDate()" >Hoy</button>`
                      //   );
                      // console.log($(".daterangepicker .drp-buttons .todayBtn"));
                    }}
                  >
                    <input
                      type="text"
                      className="form-control"
                      style={{ textAlign: "center" }}
                    />
                  </DateRangePicker>
                </Form.Group>
              </Col>
              <Col md={6}>
                <Form.Group label="Estado" className="state">
                  <Form.SelectGroup
                    pills
                    value={state}
                    onChange={(e) => {
                      if (state != e.target.value) {
                        setState(e.target.value);
                        if (e.target.value == "disposed") {
                          setDisposeDate(dateFormater(new Date(), "show"));
                        } else {
                          setDisposeDate();
                        }
                      }
                    }}
                  >
                    <Form.SelectGroupItem
                      icon="thumbs-up"
                      label="Bueno"
                      name="state"
                      value="good"
                      checked={state == "good"}
                      onChange={(e) => {}}
                    />
                    <Form.SelectGroupItem
                      icon="exchange"
                      label="Regular"
                      name="state"
                      value="regular"
                      checked={state == "regular"}
                      onChange={(e) => {}}
                    />
                    <Form.SelectGroupItem
                      icon="thumbs-down"
                      label="Mal"
                      name="state"
                      value="bad"
                      checked={state == "bad"}
                      onChange={(e) => {}}
                    />
                    <Form.SelectGroupItem
                      icon="times"
                      label="Desecho"
                      name="disposed"
                      value="disposed"
                      checked={state == "disposed"}
                      onChange={(e) => {}}
                    />
                  </Form.SelectGroup>
                </Form.Group>
              </Col>
              <Col md={6}>
                <Form.Group label="Fecha cese posesión">
                  {state != "disposed" ? (
                    <Form.Input
                      name="name"
                      className="input"
                      placeholder="Solo si cese posesión"
                      autoComplete="off"
                      type="text"
                      value=""
                      onChange={(e) => {}}
                    />
                  ) : (
                    <DateRangePicker
                      key={keyRef.current} // Needed to update the date
                      initialSettings={{
                        singleDatePicker: true,
                        opens: "center",
                        startDate: disposeDate,
                        locale: {
                          format: "DD/MM/YYYY",
                          firstDay: 1,
                          cancelLabel: textsCalendar.cancelButton[language],
                          applyLabel: textsCalendar.applyButton[language],
                          todayLabel: "Hoy",
                          customRangeLabel:
                            textsCalendar.customRangeLabel[language],
                          daysOfWeek: textsCalendar.daysOfWeek[language],
                          monthNames: textsCalendar.monthNames[language],
                        },
                        autoUpdateInput: state == "disposed",
                      }}
                      onCallback={(e) =>
                        setDisposeDate(dateFormater(e._d, "show"))
                      }
                      onShow={(e) => {
                        if (state != "disposed") {
                          console.log(e);
                        }
                        e.target.blur();
                        // if (!$(".daterangepicker .drp-buttons .todayBtn").length)
                        //   $(".daterangepicker .drp-buttons").prepend(
                        //     `<button class="todayBtn btn btn-sm btn-default" type="button" onClick="setDate()" >Hoy</button>`
                        //   );
                        // console.log($(".daterangepicker .drp-buttons .todayBtn"));
                      }}
                    >
                      <input
                        type="text"
                        className="form-control"
                        style={{ textAlign: "center" }}
                      />
                    </DateRangePicker>
                  )}
                </Form.Group>
              </Col>

              <Col md={6}>
                <Form.Group label="Marca" style={{ paddingRight: "10px" }}>
                  <Form.Input
                    name="brand"
                    className="input"
                    placeholder="Marca ..."
                    autoComplete="off"
                    type="text"
                    value={brand ? brand : ""}
                    onChange={(e) => {
                      if (e.target.value.length) {
                        setBrand(e.target.value);
                      } else {
                        setBrand();
                      }
                    }}
                  />
                </Form.Group>
              </Col>
              <Col md={6}>
                <Form.Group
                  label="Identificativo"
                  style={{ paddingRight: "10px" }}
                >
                  <Form.Input
                    name="identifier"
                    className="input"
                    placeholder="Identificativo..."
                    autoComplete="off"
                    type="text"
                    value={identifier ? identifier : ""}
                    onChange={(e) => {
                      if (e.target.value.length) {
                        setIdentifier(e.target.value);
                      } else {
                        setIdentifier();
                      }
                    }}
                  />
                </Form.Group>
              </Col>

              <Col md={9}>
                <Form.Group label="Imagen">
                  <input
                    className="with-image"
                    type="file"
                    accept="image/x-png,image/gif,image/jpeg"
                    onChange={(e) => uploadImage(e)}
                    onClick={() => setImage()}
                  />
                </Form.Group>
              </Col>
              <Col md={3}>
                <img
                  src={image}
                  style={{
                    height: "70px",
                    width: "70px",
                    objectFit: "contain",
                    float: "right",
                  }}
                />
              </Col>
              {/* <Col md={6}>
                <Form.Group label="Factura">
                  <input type="file" />
                </Form.Group>
              </Col> */}
              <Col md={12}>
                <Form.Group label="Nota">
                  <Form.Textarea
                    clasName="field-note"
                    name="note"
                    placeholder="Nota informativa..."
                    value={note ? note : ""}
                    onChange={(e) => {
                      if (e.target.value.length) {
                        setNote(e.target.value);
                      } else {
                        setNote();
                      }
                    }}
                    autoComplete="off"
                    // rows={5}
                  />
                </Form.Group>
              </Col>
            </Row>
          </div>
        </Col>
      </Row>
      <Row style={{ margin: "0px" }}>
        <Col md={12} style={{ height: "10px" }}></Col>
      </Row>
      {newItem && JSON.stringify(initialItem) != JSON.stringify(newItem) ? (
        <Row className="form-buttons" style={{ margin: "0px" }}>
          <Col md={12} style={{ padding: "0px", height: "60px" }}>
            <Row className="form-buttons" style={{ padding: "inherit" }}>
              <Col xs={6}>
                <div style={{ float: "right" }}>
                  <Button
                    pill
                    outline
                    color="info"
                    onClick={() => setFields(fetchedItem)}
                  >
                    Deshacer
                  </Button>
                </div>
              </Col>
              <Col xs={6}>
                {validForm && (
                  <div style={{ float: "left" }}>
                    <Button pill color="success" onClick={() => handleSave()}>
                      Guardar
                    </Button>
                  </div>
                )}
              </Col>
            </Row>
          </Col>
        </Row>
      ) : (
        <Row style={{ height: "60px", margin: "0" }}>
          <Col md={12} style={{ padding: "0px", height: "60px" }}></Col>
        </Row>
      )}
    </>
  );
};

export default InventoryItemForm;
