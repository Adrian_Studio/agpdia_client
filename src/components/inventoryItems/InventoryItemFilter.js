import React from "react";

import { Form, Card } from "tabler-react";
import Tree from "react-animated-tree";
import { Row, Col } from "react-bootstrap";
// import texts from "../../texts/GroupRecordFilter";
import fetchFunction from "../../helpers/fetchFuction";
import InventoryItem from "./InventoryItem";

const InventoryItemFilter = ({ parentCallBack }) => {
  const language = sessionStorage.getItem("language");
  const [error, setError] = React.useState({});
  const [locationsArray, setLocationsArray] = React.useState({
    info: [],
    filters: [],
    tree: [],
    visible: true,
  });
  const [groupsArray, setGroupsArray] = React.useState({
    info: [],
    filters: [],
    tree: [],
    visible: true,
  });
  const [stateArray, setStateArray] = React.useState({
    info: [
      { name: "Bueno", id: "good", visible: true },
      { name: "Regular", id: "regular", visible: true },
      { name: "Malo", id: "bad", visible: true },
      { name: "Desposeido", id: "disposed", visible: false },
    ],
    filters: [],
    tree: [],
    visible: false, //If you change this value it fetch multiple times
  });

  const [noteFilter, setNoteFilter] = React.useState();
  const [groups, setGroups] = React.useState();
  const [locations, setLocations] = React.useState();
  
  const fetchGroups = () => {
    var [promise, abort] = fetchFunction("inventories/groups/", "GET")
    promise.then((data) => {
      if (!data) {
        // No action
      } else {
        setGroups(data);
      }
    });

  };

  const fetchLocations = () => {
    var [promise, abort] = fetchFunction("inventories/aggs/locations/filter_list", "GET")
    promise.then((data) => {
      if (!data) {
        // No action
      } else {
        setLocations(data);
      }
    });
  };

  const formatParentCallback = () => {
    var locationsFilter = undefined;
    var groupsFilter = undefined;
    var stateFilter = undefined;
    if (!locationsArray.visible == true) {
      locationsFilter = locationsArray.filters;
    }

    if (!groupsArray.visible == true) {
      groupsFilter = groupsArray.filters;
    }
    if (!stateArray.visible == true) {
      stateFilter = stateArray.filters;
    }
    parentCallBack(locationsFilter, groupsFilter, stateFilter, noteFilter);
  };

  React.useEffect(() => {
    if (!groups) {
      fetchGroups();
    }
    if (!locations) {
      fetchLocations();
    }
  }, []);

  React.useEffect(() => {
    if (!groups || !locations) return;
    processTreeLvl1(stateArray, "setStateArray");

    var temp = { info: [], filters: [], tree: [], visible: true };

    groups.forEach((element) => {
      temp.info.push({ name: element.name, id: element._id, visible: true });
    });
    processTreeLvl1(temp, "setGroupsArray");

    var temp = { info: [], filters: [], tree: [], visible: true };

    locations.forEach((element) => {
      temp[element._id] = { info: [], tree: [] };
      element.locations.forEach((element2) => {
        temp[element._id].info.push({
          name: element2.name,
          id: element2.id,
          visible: true,
        });
      });
      temp.info.push({ name: element.name, id: element._id, visible: true });
    });
    
    processTreeLvl2(temp, "setLocationsArray");
    
  }, [groups, locations]);

  const processTreeLvl1 = (array, setArray) => {
    var temp = { ...array };
    temp.tree = [];
    temp.filters = [];
    array.info.forEach((element) => {
      if (element.visible) {
        temp.filters.push(element.id);
      }
      temp.tree.push(
        <Tree
          key={element.id}
          content={element.name}
          visible={element.visible}
          canHide
          onClick={(e) => handleClick(temp, 1, 1, setArray, e, element.id)}
        />
      );
    });
    eval(setArray)(temp);
  };
  const processTreeLvl2 = (array, setArray) => {
    var temp = { ...array };
    temp.tree = [];
    temp.filters = [];
    temp.info.forEach((element) => {
      temp[element.id].tree = [];
      temp[element.id].info.forEach((element2) => {
        if (element2.visible) {
          temp.filters.push(element2.id);
        }
        temp[element.id].tree.push(
          <Tree
            key={element2.id}
            content={element2.name}
            visible={element2.visible}
            canHide
            onClick={(e) =>
              handleClick(temp, 2, 2, setArray, e, element2.id, element.id)
            }
          />
        );
      });

      temp.tree.push(
        <Tree
          key={element.id}
          content={element.name}
          visible={element.visible}
          canHide
          onClick={(e) => handleClick(temp, 1, 2, setArray, e, element.id)}
        >
          {temp[element.id].tree}
        </Tree>
      );
    });
    setLocationsArray(temp);
  };
  React.useEffect(() => {
    formatParentCallback();
  }, [locationsArray, groupsArray, stateArray, noteFilter]);

  const handleClick = (array, position, lvl, setArray, state, id, parentId) => {
    if (position == 0 && lvl >= 1) {
      array.info.forEach((element) => {
        if (lvl == 2) {
          array[element.id].info.forEach((element) => {
            element.visible = state;
          });
        }
        element.visible = state;
      });
      array.visible = state;
    }
    if (position == 1 && lvl >= 1) {
      var index = array.info.findIndex((x) => x.id == id);
      array.info[index].visible = state;
    }
    if (position == 1 && lvl == 2) {
      array[id].info.forEach((element) => {
        element.visible = state;
      });
    }

    if (position == 2 && lvl == 2) {
      var index2 = array[parentId].info.findIndex((x) => x.id == id);
      array[parentId].info[index2].visible = state;
      var parent1Visible = true;
      array[parentId].info.forEach((element) => {
        parent1Visible = parent1Visible && element.visible;
      });
      var index3 = array.info.findIndex((x) => x.id == parentId);
      array.info[index3].visible = parent1Visible;
    }
    if (position >= 1 && lvl >= 1) {
      var parent0Visible = true;
      array.info.forEach((element) => {
        parent0Visible = parent0Visible && element.visible;
      });
      array.visible = parent0Visible;
    }

    eval("processTreeLvl" + lvl)(array, setArray);
  };

  return (
    <Card title="Elementos" isCollapsible>
      {locations && groups && (
        <Card.Body>
          <Row>
            <Col lg={12} md={12}>
              <Form.Group label="Filtro por texto">
                <Form.Input
                  name="noteFilter"
                  autoComplete="off"
                  value={noteFilter}
                  placeholder="Texto a buscar..."
                  onChange={(e) => setNoteFilter(e.target.value)}
                />
              </Form.Group>
              <br />
            </Col>

            <Col lg={12} md={4}>
              <Row>
                <Col md={12}>
                  <Tree
                    content="Grupo"
                    type={
                      groupsArray.visible
                        ? "ALL"
                        : groupsArray.filters.length
                        ? "SOME"
                        : "NONE"
                    }
                    canHide
                    visible={groupsArray.visible}
                    onClick={(e) =>
                      handleClick(groupsArray, 0, 1, "setGroupsArray", e)
                    }
                  >
                    {groupsArray.tree}
                  </Tree>
                  <br />
                </Col>
              </Row>
            </Col>

            <Col lg={12} md={4}>
              <Row>
                <Col md={12}>
                  <Tree
                    content="Estado"
                    type={
                      stateArray.visible
                        ? "ALL"
                        : stateArray.filters.length
                        ? "SOME"
                        : "NONE"
                    }
                    canHide
                    visible={stateArray.visible}
                    onClick={(e) =>
                      handleClick(stateArray, 0, 1, "setStateArray", e)
                    }
                  >
                    {stateArray.tree}
                  </Tree>
                  <br />
                </Col>
              </Row>
            </Col>
            
            <Col lg={12} md={4}>
              <Tree
                content="Ubicación"
                type={
                  locationsArray.visible
                    ? "ALL"
                    : locationsArray.filters.length
                    ? "SOME"
                    : "NONE"
                }
                canHide
                visible={locationsArray.visible}
                onClick={(e) =>
                  handleClick(locationsArray, 0, 2, "setLocationsArray", e)
                }
              >
                {locationsArray.tree}
              </Tree>
              <br />
            </Col>
          </Row>
        </Card.Body>
      )}
    </Card>
  );
};

export default InventoryItemFilter;
