import React from "react";

import { Card, Col, Row } from "react-bootstrap";
import currencyFormater from "../../helpers/currencyFormater";
import { Form, Button } from "tabler-react";

import fetchFunction from "../../helpers/fetchFuction";

import Swal from "sweetalert2";
const AccountRecordTotal = ({
  props,
  totalCheckBoxHandler,
  showModalHandler,
  refetchAfterDelete,
  updateSignal
}) => {
  const main_currency_symbol = sessionStorage.getItem("main_currency_symbol");
  const main_currency_format = sessionStorage.getItem("main_currency_format");

  const amountStyle = {
    display: "flex",
    alignItems: "center",
    fontSize: "18px",
    justifyContent: "center",
    padding: "0.75rem",
    textAlign: "right",
  };
  const switchStyle = {
    fontSize: "14px",
    display: "flex",
    alignItems: "center",
    fontWeight: "600",
    padding: "0.75rem",
  };
  const infoStyle = {
    fontSize: "14px",
    // display: "flex",
    alignItems: "center",
    fontWeight: "600",
    padding: "0.25rem",
    textAlign: "center",
  };
  const selectedTextStyle = {
    fontSize: "14px",
    display: "flex",
    alignItems: "center",
    fontWeight: "600",
    padding: "0.25rem",
    textAlign: "center",
  };

  const handleChange = (e) => {
    totalCheckBoxHandler(e.target.checked);
  };

  const handleClick = (toAdd) => {
    showModalHandler(undefined, toAdd);
  };

  const deleteItems = () => {
    Swal.fire({
      title: "¿Estas seguro de que proceder con la eliminación?",
      text: `${props.selected.length} ${
        props.selected.length == 1 ? " registro" : " registros"
      } a eliminar`,
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Si, !Borrar!",
    }).then((result) => {
      if (!result.value) return;
      var [promise, abort] = fetchFunction("accounts/records/" + props.selected, "DELETE")
      promise.then((data) => {
        if (!data) {
          Swal.fire({
            text: `No se han encontrado los registros a eliminar o estan en uso`,
            icon: "error",
            timer: "2000",
          });
        } else {
          refetchAfterDelete();
            updateSignal();
            Swal.fire({
              text: `${
                props.selected.length == 1
                  ? `El registro se ha eliminado`
                  : `Los ${props.selected.length} registros se han eliminado`
              }`,
              icon: "success",
              timer: "2000",
            });
        }
      });
    });
  };

  return (
    <Card
      className="account-record-total"
      style={{
        marginBottom: "0.1rem",
        background: "ghostwhite",
        minHeight: "60px",
      }}
    >
      <Card.Body style={{ padding: "0px 10px", display: "flex" }}>
        <Col md={9} xs={7}>
          {props ? (
            <Row style={{ height: "100%" }}>
              <Col md={4} xs={4} style={switchStyle}>
                <Form.Switch
                  name="total_switch"
                  value="total"
                  onChange={handleChange}
                  checked={props.isChecked}
                />
                <span className="d-none d-md-flex">
                  {props.selected.length ? "Deselect all" : "Select all"}
                </span>
              </Col>
              <Col md={8} xs={8} style={{ margin: "auto" }}>
                {!props.selected.length ? (
                  <Row style={{ height: "100%" }}>
                    <Col md={12} xs={12} style={infoStyle}>
                      <Button
                        pill
                        outline
                        color="primary"
                        onClick={() => handleClick(true)}
                      >
                        Añadir
                      </Button>
                    </Col>
                  </Row>
                ) : (
                  <Row style={{ height: "100%" }}>
                    <Col md={4} xs={12} style={selectedTextStyle}>
                      <div style={{ width: "inherit" }}>
                        <strong>
                          {props.selected.length} Registro
                          {props.selected.length > 1 ? "s" : ""}
                        </strong>
                      </div>
                    </Col>
                    {props.selected.length == 1 && (!props.toShow || props.toShow.includes("all")) ? (
                      <Col md={4} xs={12} style={infoStyle}>
                        <Button
                          pill
                          outline
                          color="info"
                          onClick={() => handleClick(true)}
                        >
                          Copiar
                        </Button>
                      </Col>
                    ) : (
                      <Col md={4} xs={12} style={infoStyle}>
                        {/* Overcomplicated deal with multiple formats of records at the same time, so it isn't programmed
                         <Button
                          pill
                          outline
                          color="warning"
                          onClick={() => handleClick(false)}
                        >
                          Editar
                        </Button> */}
                      </Col>
                    )}
                    <Col md={4} xs={12} style={infoStyle}>
                      <Button
                        pill
                        outline
                        color="danger"
                        onClick={(e) => deleteItems(e)}
                      >
                        Borrar
                      </Button>
                    </Col>
                  </Row>
                )}
              </Col>
            </Row>
          ) : (
            <Row style={{ height: "100%" }}>
              <Col md={4} xs={4} style={switchStyle}></Col>
              <Col md={8} xs={8} style={{ margin: "auto" }}>
                <Row style={{ height: "100%" }}>
                  <Col md={12} xs={12} style={infoStyle}>
                    <Button
                      pill
                      outline
                      color="primary"
                      onClick={() => handleClick(true)}
                    >
                      Añadir
                    </Button>
                  </Col>
                </Row>
              </Col>
            </Row>
          )}
        </Col>
        <Col md={3} xs={5} style={amountStyle}>
          <div style={{ width: "inherit" }}>
            {props && props.amount ? (
              <strong>
                {currencyFormater(
                  props.amount,
                  main_currency_format,
                  main_currency_symbol
                )}
              </strong>
            ) : props ? (
              <strong>
                {currencyFormater(
                  props.amount,
                  main_currency_format,
                  main_currency_symbol
                )}{" "}
              </strong>
            ) : (
              <strong>Sin registros</strong>
            )}
          </div>
        </Col>
      </Card.Body>
    </Card>
  );
};

export default AccountRecordTotal;
