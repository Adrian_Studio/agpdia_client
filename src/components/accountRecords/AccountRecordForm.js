import React, { useEffect, useState, useRef } from "react";

import moment from "moment";
import texts from "../../texts/AccountRecordForm";
import textsCalendar from "../../texts/Calendar";
import { Modal, Col, Row } from "react-bootstrap";
import DateRangePicker from "react-bootstrap-daterangepicker";
import { Form, Button, Icon } from "tabler-react";
import Select from "react-select";
import fetchFunction from "../../helpers/fetchFuction";
import dateFormater from "../../helpers/dateFormater";
import $ from "jquery";

const AccountRecordForm = ({ props, modalSaveHandler }) => {
  const language = sessionStorage.getItem("language");
  const [type, setType] = useState("expense");
  const [currencySymbol, setCurrencySymbol] = useState("€");
  // const [currencySymbolTo, setCurrencySymbolTo] = useState("€");
  const [hideCategorySelect, setHideCategorySelect] = useState(false);
  const [initialRecord, setInitialRecord] = useState(false);
  const [fetchedRecord, setFetchedRecord] = useState(false);
  const [newRecord, setNewRecord] = useState(false);
  const [validForm, setValidForm] = useState();

  const [accountsOptions, setAccountsOptions] = useState();
  const [housesOptions, setHousesOptions] = useState();
  const [vehiclesOptions, setVehiclesOptions] = useState();
  const [categoriesOptions, setCategoriesOptions] = useState();
  // const [conceptsOptions, setConceptsOptions] = useState();
  const [allConceptsOptions, setAllConceptsOptions] = useState();

  const [category, setCategory] = useState();
  const [group, setGroup] = useState();
  const [subgroup, setSubgroup] = useState();
  const [consumptionUd, setConsumptionUd] = useState();

  const [concept, setConcept] = useState();
  const [note, setNote] = useState();
  const [amount, setAmount] = useState();
  // const [amountTo, setAmountTo] = useState();
  const [account, setAccount] = useState();
  const [accountTo, setAccountTo] = useState();
  const [vehicle, setVehicle] = useState();
  const [house, setHouse] = useState();
  const [date, setDate] = useState();
  const [grossAmount, setGrossAmount] = useState();
  const [consumption, setConsumption] = useState();
  const [milometer, setMilometer] = useState();
  const [transferType, setTransferType] = useState();

  const keyRef = useRef(Date.now()); // Needed to update the date
  keyRef.current = Date.now(); // Needed to update the date

  const fetchRecord = (objectId) => {
    var [promise, abort] = fetchFunction("accounts/records/" + objectId, "GET")
    promise.then((data) => {
      if (!data) {
        // No action
      } else {
        setFetchedRecord(data);
        setFields(data);
      }
    });
  };

  const handleSave = () => {
    var processedNewRecord = newRecord;

    switch (props.modalType) {
      case "Add":
      case "Copy":
        processedNewRecord.date = dateFormater(
          processedNewRecord.date,
          "database"
        );
        var [promise, abort] = fetchFunction("accounts/records/", "POST", newRecord)
        promise.then((data) => {
          if (!data) {
            // No action
          } else {
            modalSaveHandler();
          }
        });
        break;
      case "UpdateOne":
        var objectId = props.objectId[0];
        processedNewRecord.date = dateFormater(
          processedNewRecord.date,
          "database"
        );
        var [promise, abort] = fetchFunction("accounts/records/" + objectId, "PUT", processedNewRecord)
        promise.then((data) => {
          if (!data) {
            // No action
          } else {
            modalSaveHandler();
          }
        });  
        break;
      // Update many should be here but it is overcomplicated and innecesary so it isnt called in AccountRecordTotal.
      default:
        break;
    }
  };

  const setFields = (data) => {
    if (data) {
      // setConcepts(data);
      if (data.type == "info") {
        var typeTemp = "expense";
      } else {
        var typeTemp = data.type;
      }

      setType(typeTemp);
      setAccount({ value: data.account_id, label: data.account_name });
      setAccountTo(
        data.account_to_id && {
          value: data.account_to_id,
          label: data.account_to_name,
        }
      );
      setAmount(data.amount && data.amount.toString().replace(",", "."));
      setConcept({ value: data.concept_id, label: data.concept_name });
      setCategory({ value: data.category_id, label: data.category_name });
      setHideCategorySelect(true);
      if (data.vehicle_id)
        setVehicle({ value: data.vehicle_id, label: data.vehicle_name });
      if (data.house_id)
        setHouse({ value: data.house_id, label: data.house_name });
      setNote(data.note);
      setDate(dateFormater(data.date, "show"));
      setConsumption(
        data.consumption && data.consumption.toString().replace(",", ".")
      );
      setConsumptionUd(data.consumption_ud);
      setGrossAmount(
        data.gross_amount && data.gross_amount.toString().replace(",", ".")
      );
      setMilometer(data.milometer && data.milometer.toString());
      var data_tranferType = undefined;

      console.log(data)
      if (data.concept_group == "stock") {
        data_tranferType = "stock";
        setTransferType("stock");
      } else {
        setTransferType();
      }
      console.log(data);
      var backData = {
        concept_id: data.concept_id,
        note: data.note,
        amount: data.amount && data.amount.toString().replace(",", "."),
        account_id: data.account_id,
        account_to_id: data.account_to_id,
        vehicle_id: data.vehicle_id,
        house_id: data.house_id,
        date: dateFormater(data.date, "show"),
        gross_amount:
          data.gross_amount && data.gross_amount.toString().replace(",", "."),
        consumption:
          data.consumption && data.consumption.toString().replace(",", "."),
        consumption_ud: data.consumption_ud,
        milometer: data.milometer && data.milometer.toString(),
        type: typeTemp,
        transfer_type: data_tranferType,
      };

      // setConceptsOptions(allConceptsOptions[data.category_id])
    } else {
      setAccount();
      setAccountTo();
      setAmount();
      setConcept();
      setCategory();
      setHideCategorySelect(false);
      setVehicle();
      setHouse();
      setNote();
      setDate(dateFormater(new Date(), "show"));
      setConsumption();
      setConsumptionUd();
      setGrossAmount();
      setMilometer();
      setTransferType();

      var backData = {
        concept_id: undefined,
        note: undefined,
        amount: undefined,
        account_id: undefined,
        account_to_id: undefined,
        vehicle_id: undefined,
        house_id: undefined,
        date: dateFormater(new Date(), "show"),
        gross_amount: undefined,
        consumption: undefined,
        consumption_ud: undefined,
        milometer: undefined,
        type: type,
        transfer_type: undefined,
      };
    }

    setInitialRecord(backData);
    setNewRecord(backData);
  };

  React.useEffect(() => {
    if (props.accounts) {
      var tempAccounts = [];
      props.accounts.forEach((account) => {
        tempAccounts.push({
          label: account.name,
          value: account._id,
        });
      });
      setAccountsOptions(tempAccounts);
    }

    if (props.houses) {
      var tempHouses = [];
      props.houses.forEach((house) => {
        tempHouses.push({
          label: house.name,
          value: house._id,
        });
      });
      setHousesOptions(tempHouses);
    }

    if (props.vehicles) {
      var tempVehicles = [];
      props.vehicles.forEach((vehicle) => {
        tempVehicles.push({
          label: vehicle.name,
          value: vehicle._id,
        });
      });
      setVehiclesOptions(tempVehicles);
    }

    if (props.concepts) {
      var tempCategories = [];
      var tempConcepts = {};
      props.concepts.forEach((category) => {
        tempCategories.push({ label: category.name, value: category._id });
        tempConcepts[category._id] = [];
        // tempConcepts[undefined] = [<option></option>];
        category.concepts.forEach((concept) => {
          tempConcepts[category._id].push({
            label: concept.name,
            value: concept.id,
          });
        });
      });
      setCategoriesOptions(tempCategories);
      setAllConceptsOptions(tempConcepts);
    }

    if (
      (props.modalType == "UpdateOne" || props.modalType == "Copy") &&
      props.objectId.length &&
      props.objectId[0].length
    ) {
      fetchRecord(props.objectId);
    } else if (props.modalType == "Add" && !props.objectId.length) {
      setFields();
    }
  }, []);

  React.useEffect(() => {
    if (!concept || type == "transfer") return;
    var conceptInfo = props.conceptsInfo.find(
      (conceptInfo) => conceptInfo._id == concept.value
    );

    if (!conceptInfo){console.log(`ERROR: Tiene concepto pero no ha encontrado la informacion del concepto. Concepto buscado: ${concept}`)}
    setGroup(conceptInfo.group);
    if (conceptInfo.group != "vehicle"){
      setVehicle();
    } else{
      setVehicle(vehiclesOptions[0]);
    }
    if (conceptInfo.group != "house"){
      setHouse();
    }else{
      setHouse(housesOptions[0]);
    }
    
    if (conceptInfo.subgroup != "paysheet") setGrossAmount();
    if (conceptInfo.subgroup != "paysheet") setGrossAmount();
    if (
      conceptInfo.subgroup != "water_house" &&
      conceptInfo.subgroup != "light_house" &&
      conceptInfo.subgroup != "gas_house" &&
      conceptInfo.subgroup != "power_source_vehicle"
    ) {
      setConsumption();
      setConsumptionUd();
    }
    if (conceptInfo.subgroup == "water_house") {
      setConsumptionUd("m³");
    }
    if (
      conceptInfo.subgroup == "light_house" ||
      conceptInfo.subgroup == "gas_house"
    ) {
      setConsumptionUd("kWh");
    }
    if (conceptInfo.subgroup == "power_source_vehicle") {
      setConsumptionUd("L");
    }
    if (
      conceptInfo.subgroup != "maintenance_vehicle" &&
      conceptInfo.subgroup != "power_source_vehicle"
    ) {
      setMilometer();
    }

    setSubgroup(conceptInfo.subgroup);
  }, [concept]);

  React.useEffect(() => {
    if (type != "income") {
      setAmount(amount && "-" + amount.toString().replace("-", ""));
    } else {
      setAmount(amount && amount.toString().replace("-", ""));
    }
    if (type == "transfer") {
      setGroup();
      setConcept();
      setSubgroup();
      setVehicle();
      setHouse();
      setGrossAmount();
      setConsumption();
      setConsumptionUd();
      setMilometer();
    }
  }, [type]);

  React.useEffect(() => {
    var backData = {
      concept_id: concept ? concept.value : undefined,
      note: note,
      amount: amount && amount.toString().replace(",", "."),
      account_id: account ? account.value : undefined,
      account_to_id: accountTo ? accountTo.value : undefined,
      vehicle_id: vehicle ? vehicle.value : undefined,
      house_id: house ? house.value : undefined,
      date: date,
      gross_amount: grossAmount && grossAmount.toString().replace(",", "."),
      consumption: consumption && consumption.toString().replace(",", "."),
      consumption_ud: consumption && consumptionUd,
      milometer: milometer && milometer.toString(),
      type: type,
      transfer_type: transferType,
    };
    if (
      // props.modalType != "UpdateMany" &&
      date &&
      account &&
      /^(\-)?[0-9]{0,9}([\.\,]([0-9]{1,2})?)?$/i.test(amount) &&
      (type != "transfer" ||
        (accountTo &&
          accountTo.value !=
            account.value)) /* && /^(\-)?[0-9]{0,9}([\.\,]([0-9]{1,2})?)?$/i.test(amountTo)*/ &&
      (type == "transfer" || concept) &&
      (group != "house" ||( house && house.value && group == "house")) &&
      (group != "vehicle" ||( vehicle && vehicle.value && group == "vehicle"))
    ) {
      setValidForm(true);
      // } else if (props.modalType == "UpdateMany" && (account || concept || note)) {
      //   setValidForm(true);
    } else {
      setValidForm(false);
    }
    setNewRecord(backData);
    if (props.toShow && props.toShow.includes("stock")) {
      console.log("ENTRA 2")
      setTransferType("stock");
    }
  }, [
    concept,
    note,
    amount,
    // amountTo,
    account,
    accountTo,
    vehicle,
    house,
    date,
    grossAmount,
    consumption,
    consumptionUd,
    milometer,
    type,
    transferType,
    group,
  ]);

  return (
    <>
      {/* {props.modalType == "UpdateMany" ? (
        <Row style={{ margin: "10px 0 0 0" }} className="accountForm coloredForm">
          <Col md={8}>
            <Row>
              <Col md={6}>
                <Form.Group label={texts.accountSelect[language] + " *"}>
                  <div style={{ color: "rgb(73, 80, 87)" }}>
                    <Select
                      onChange={(e) => {
                        setAccount(e);
                      }}
                      isSearchable={false}
                      value={{
                        label: account
                          ? account.label
                          : "Selecciona " + texts.accountSelect[language],
                      }}
                      options={accountsOptions}
                    />
                  </div>
                </Form.Group>
              </Col>
              <Col md={6}>
                <Form.Group
                  label={
                    hideCategorySelect
                      ? // ? "Cat. " + category.label + " → " + texts.conceptSelect[language]
                        category.label + " *"
                      : texts.categorySelect[language] + " *"
                  }
                >
                  <Select
                    openMenuOnFocus={hideCategorySelect}
                    onChange={(e) => {
                      if (hideCategorySelect && e.value) {
                        setConcept(e);
                      } else {
                        setCategory(e);
                        setHideCategorySelect(true);
                      }
                    }}
                    onMenuOpen={(e) => {
                      if (concept) {
                        setCategory();
                        setConcept();
                        setHideCategorySelect(false);
                      }
                    }}
                    // onMenuClose={(e)=>{if(!concept) setMenuConceptOpen(true)}}
                    menuIsOpen={category && !concept}
                    // isSearchable={false}
                    // isClearable
                    value={{
                      label: concept
                        ? concept.label
                        : "Selecciona " +
                          (hideCategorySelect
                            ? texts.conceptSelect[language]
                            : texts.categorySelect[language]),
                    }}
                    // closeMenuOnSelect={hideCategorySelect}
                    // value={concept}
                    options={
                      hideCategorySelect && !concept
                        ? category
                          ? allConceptsOptions[category.value]
                          : { label: "Seleccionar Concepto" }
                        : categoriesOptions
                    }
                  />
                </Form.Group>
              </Col>
            </Row>
            <Row>
              <Col md={12}>
                <Form.Group label={texts.noteTextArea[language]}>
                  <Form.Textarea
                    clasName="field-note"
                    name="note"
                    placeholder="Nota informativa..."
                    value={note}
                    onChange={(e) => {
                      setNote(e.target.value);
                    }}
                    autoComplete="off"
                    // rows={5}
                  />
                </Form.Group>
              </Col>
            </Row>
          </Col>

          <Col md={4} style={{ display: "flex", alignItems: "center" }}>
            {validForm && (
              <Row style={{ margin: "auto" }}>
                <Col xs={6}>
                  <div style={{ float: "right" }}>
                    <Button
                      pill
                      outline
                      color="info"
                      onClick={() => setFields(fetchedRecord)}
                    >
                      Deshacer
                    </Button>
                  </div>
                </Col>
                <Col xs={6}>
                  {validForm && (
                    <div style={{ float: "left" }}>
                      <Button pill color="success" onClick={() => handleSave()}>
                        Guardar
                      </Button>
                    </div>
                  )}
                </Col>
              </Row>
            )}
          </Col>
        </Row>
      ) : ( */}
      <>
        <Row style={{ margin: "0px" }} className="accountForm coloredForm">
          <Col md={8} style={{ padding: "0px" }}>
            <div
              style={{
                background: "rgb(144, 164, 174)",
                color: "white",
                padding: "1rem",
              }}
            >
              <Row>
                <Col md={12}>
                  <div style={{ width: "300px", margin: "auto" }}>
                    <Form.Group className="type">
                      <Form.SelectGroup
                        value={type}
                        onChange={(e) => {
                          if (
                            type == "transfer" &&
                            e.target.value != "transfer"
                          ) {
                            setAccountTo();
                            setConcept();
                            setCategory();
                            setHideCategorySelect(false);
                          }
                          setType(e.target.value);
                        }}
                      >
                        <Form.SelectGroupItem
                          label={texts.typeSelectExpense[language]}
                          name="type"
                          value="expense"
                          checked={type == "expense"}
                          onChange={(e) => {}}
                        />
                        <Form.SelectGroupItem
                          label={texts.typeSelectIncome[language]}
                          name="type"
                          value="income"
                          checked={type == "income"}
                          onChange={(e) => {}}
                        />
                        {/* {props.toShow && props.toShow.some((r) =>
                          ["all", "stock"].includes(r)
                        ) && ( */}
                          <Form.SelectGroupItem
                            label={texts.typeSelectTransfer[language]}
                            name="type"
                            value="transfer"
                            checked={type == "transfer"}
                            onChange={(e) => {}}
                          />
                        {/* )} */}
                      </Form.SelectGroup>
                    </Form.Group>
                  </div>
                </Col>
              </Row>

              {type != "transfer" ? (
                <Row>
                  <Col md={12}>
                    <div style={{ width: "208px", margin: "auto" }}>
                      <Form.Group label={texts.accountSelect[language] + " *"}>
                        <div style={{ color: "rgb(73, 80, 87)" }}>
                          <Select
                            onChange={(e) => {
                              setAccount(e);
                            }}
                            isSearchable={false}
                            value={{
                              label: account
                                ? account.label
                                : "Selecciona " + texts.accountSelect[language],
                            }}
                            options={accountsOptions}
                          />
                        </div>
                      </Form.Group>
                    </div>
                  </Col>
                  <Col md={12}>
                    <div
                      style={{
                        width: "208px",
                        margin: "auto",
                        display: "flex",
                      }}
                    >
                      <div
                        style={{
                          width: "135px",
                          display: "flex",
                          paddingRight: "12px",
                        }}
                      >
                        <Form.Group
                          label={texts.amountInput[language] + " *"}
                          className="field-amount expense"
                          style={{ paddingRight: "10px" }}
                        >
                          <Form.Input
                            icon={
                              type == "expense"
                                ? "minus"
                                : type == "income"
                                ? "plus"
                                : ""
                            }
                            name="amount"
                            className="input"
                            placeholder="0,00"
                            type="text"
                            value={
                              amount != undefined
                                ? amount
                                    .toString()
                                    .replace(".", ",")
                                    .replace("-", "")
                                : ""
                            }
                            onChange={(e) => {
                              var temp = e.target.value;

                              if (
                                /^[0-9]{0,9}([\.\,]([0-9]{1,2})?)?$/i.test(temp)
                              )
                                temp
                                  ? setAmount(
                                      type == "expense" ? "-" + temp : temp
                                    )
                                  : setAmount();
                            }}
                            autoComplete="off"
                          />
                        </Form.Group>
                      </div>
                      <div
                        style={{ width: "73px", justifyContent: "flex-end" }}
                      >
                        <Form.Group label={texts.currencySelect[language]}>
                          <Form.Input
                            name="currencySymbol"
                            className="input"
                            value={currencySymbol}
                            readOnly
                          />
                        </Form.Group>
                      </div>
                    </div>
                  </Col>
                </Row>
              ) : (
                <Row>
                  <Col
                    md={5}
                    style={{ paddingLeft: "20px", paddingRight: "0px" }}
                  >
                    <Row>
                      <Col md={12}>
                        <Form.Group label="Cuenta origen *">
                          <div style={{ color: "rgb(73, 80, 87)" }}>
                            <Select
                              onChange={(e) => {
                                setAccount(e);
                              }}
                              isSearchable={false}
                              value={{
                                label: account
                                  ? account.label
                                  : "Selecciona " +
                                    texts.accountSelect[language],
                              }}
                              options={accountsOptions}
                            />
                          </div>
                        </Form.Group>
                      </Col>
                      <Col md={12} style={{ display: "flex" }}>
                        <div
                          style={{
                            width: "135px",
                            display: "flex",
                            paddingRight: "12px",
                          }}
                        >
                          <Form.Group
                            label={texts.amountInput[language] + " *"}
                            className="field-amount expense"
                            style={{ paddingRight: "10px" }}
                          >
                            <Form.Input
                              icon="minus"
                              name="amount"
                              className="input"
                              placeholder="0,00"
                              type="text"
                              value={
                                amount != undefined
                                  ? amount
                                      .toString()
                                      .replace(".", ",")
                                      .replace("-", "")
                                  : ""
                              }
                              onChange={(e) => {
                                var temp = e.target.value;

                                if (
                                  /^[0-9]{0,9}([\.\,]([0-9]{1,2})?)?$/i.test(
                                    temp
                                  )
                                )
                                  temp ? setAmount("-" + temp) : setAmount();
                              }}
                              autoComplete="off"
                            />
                          </Form.Group>
                        </div>
                        <div
                          style={{ width: "73px", justifyContent: "flex-end" }}
                        >
                          <Form.Group label={texts.currencySelect[language]}>
                            <Form.Input
                              name="currencySymbol"
                              className="input"
                              value={currencySymbol}
                              readOnly
                            />
                          </Form.Group>
                        </div>
                      </Col>
                    </Row>
                  </Col>

                  <Col md={2} style={{ display: "flex", alignItems: "center" }}>
                    <div style={{ textAlign: "center", width: "100%" }}>
                      <Icon name="arrow-right" />
                    </div>
                  </Col>

                  <Col
                    md={5}
                    style={{ paddingLeft: "0px", paddingRight: "20px" }}
                  >
                    <Row>
                      <Col md={12}>
                        <Form.Group label="Cuenta destino *">
                          <div style={{ color: "rgb(73, 80, 87)" }}>
                            <Select
                              onChange={(e) => {
                                setAccountTo(e);
                              }}
                              isSearchable={false}
                              value={{
                                label: accountTo
                                  ? accountTo.label
                                  : "Selecciona " +
                                    texts.accountSelect[language],
                              }}
                              options={accountsOptions}
                            />
                          </div>
                        </Form.Group>
                      </Col>
                      <Col md={12} style={{ display: "flex" }}>
                        <div
                          style={{
                            width: "135px",
                            display: "flex",
                            paddingRight: "12px",
                          }}
                        >
                          <Form.Group
                            label={texts.amountInput[language] + " *"}
                            className="field-amount expense"
                            style={{ paddingRight: "10px" }}
                          >
                            <Form.Input
                              icon="plus"
                              name="amountTo"
                              className="input"
                              placeholder="0,00"
                              type="text"
                              value={
                                accountTo != undefined && amount != undefined
                                  ? amount
                                      .toString()
                                      .replace(".", ",")
                                      .replace("-", "")
                                  : ""
                              }
                              onChange={(e) => {
                                var temp = e.target.value;

                                if (
                                  /^[0-9]{0,9}([\.\,]([0-9]{1,2})?)?$/i.test(
                                    temp
                                  )
                                )
                                  temp ? setAmount("-" + temp) : setAmount();
                              }}
                              autoComplete="off"
                            />
                          </Form.Group>
                        </div>
                        <div
                          style={{ width: "73px", justifyContent: "flex-end" }}
                        >
                          <Form.Group label={texts.currencySelect[language]}>
                            <Form.Input
                              name="currencySymbolTo"
                              className="input"
                              value={currencySymbol}
                              readOnly
                            />
                          </Form.Group>
                        </div>
                      </Col>
                    </Row>
                  </Col>
                </Row>
              )}
            </div>
            <div style={{ padding: "1rem" }}>
              <Row>
                {type == "transfer" ? (
                  <Col md={3}></Col>
                ) : (
                  <Col md={6}>
                    <Form.Group
                      label={
                        hideCategorySelect
                          ? // ? "Cat. " + category.label + " → " + texts.conceptSelect[language]
                            category.label + " *"
                          : texts.categorySelect[language] + " *"
                      }
                    >
                      <Select
                        openMenuOnFocus={hideCategorySelect}
                        onChange={(e) => {
                          if (hideCategorySelect && e.value) {
                            setConcept(e);
                          } else {
                            setCategory(e);
                            setHideCategorySelect(true);
                          }
                        }}
                        onMenuOpen={(e) => {
                          if (concept) {
                            setCategory();
                            setConcept();
                            setHideCategorySelect(false);
                          }
                        }}
                        // onMenuClose={(e)=>{if(!concept) setMenuConceptOpen(true)}}
                        menuIsOpen={category && !concept}
                        // isSearchable={false}
                        // isClearable
                        value={{
                          label: concept
                            ? concept.label
                            : "Selecciona " +
                              (hideCategorySelect
                                ? texts.conceptSelect[language]
                                : texts.categorySelect[language]),
                        }}
                        // closeMenuOnSelect={hideCategorySelect}
                        // value={concept}
                        options={
                          hideCategorySelect && !concept
                            ? category
                              ? allConceptsOptions[category.value]
                              : { label: "Seleccionar Concepto" }
                            : categoriesOptions
                        }
                      />
                    </Form.Group>
                  </Col>
                )}

                <Col md={6}>
                  <Form.Group label={texts.dateSelect[language] + " *"}>
                    <DateRangePicker
                      key={keyRef.current} // Needed to update the date
                      initialSettings={{
                        singleDatePicker: true,
                        opens: "center",
                        startDate: date,
                        locale: {
                          format: "DD/MM/YYYY",
                          firstDay: 1,
                          cancelLabel: textsCalendar.cancelButton[language],
                          applyLabel: textsCalendar.applyButton[language],
                          todayLabel: "Hoy",
                          customRangeLabel:
                            textsCalendar.customRangeLabel[language],
                          daysOfWeek: textsCalendar.daysOfWeek[language],
                          monthNames: textsCalendar.monthNames[language],
                        },
                      }}
                      onCallback={(e) => setDate(dateFormater(e._d, "show"))}
                      onShow={(e) => {
                        e.target.blur();
                        // if (!$(".daterangepicker .drp-buttons .todayBtn").length)
                        //   $(".daterangepicker .drp-buttons").prepend(
                        //     `<button class="todayBtn btn btn-sm btn-default" type="button" onClick="setDate()" >Hoy</button>`
                        //   );
                        // console.log($(".daterangepicker .drp-buttons .todayBtn"));
                      }}
                    >
                      <input
                        type="text"
                        className="form-control"
                        style={{ textAlign: "center" }}
                      />
                    </DateRangePicker>
                  </Form.Group>
                </Col>
              </Row>
            </div>
          </Col>

          <Col md={4} style={{ backgroundColor: "#eff0f2" }}>
            <div style={{ paddingTop: "1rem" }}></div>
            {group == "house" && (
              <Row>
                <Col md={12}>
                  <Form.Group label={texts.houseSelect[language]}>
                    <div style={{ color: "rgb(73, 80, 87)" }}>
                      <Select
                        onChange={(e) => {
                          setHouse(e);
                        }}
                        isSearchable={false}
                        value={{
                          label: house
                            ? house.label
                            : "Selecciona " + texts.houseSelect[language],
                        }}
                        options={housesOptions}
                      />
                    </div>
                  </Form.Group>
                </Col>
              </Row>
            )}
            {group == "vehicle" && (
              <Row>
                <Col md={12}>
                  <Form.Group label={texts.vehicleSelect[language]}>
                    <div style={{ color: "rgb(73, 80, 87)" }}>
                      <Select
                        onChange={(e) => {
                          setVehicle(e);
                        }}
                        isSearchable={false}
                        value={{
                          label: vehicle
                            ? vehicle.label
                            : "Selecciona " + texts.vehicleSelect[language],
                        }}
                        options={vehiclesOptions}
                      />
                    </div>
                  </Form.Group>
                </Col>
              </Row>
            )}
            {subgroup == "paysheet" && (
              <Row>
                <Col md={12}>
                  <Form.Group
                    label="Gross Amount"
                    className="field-amount"
                    //style={{ paddingRight: "10px" }}
                  >
                    <Form.Input
                      name="grossAmount"
                      className="input"
                      placeholder="0,00"
                      autoComplete="off"
                      type="text"
                      value={
                        grossAmount != undefined
                          ? grossAmount.toString().replace(".", ",")
                          : ""
                      }
                      onChange={(e) => {
                        var temp = e.target.value;
                        if (/^[0-9]{0,9}([\.\,]([0-9]{1,2})?)?$/i.test(temp)) {
                          temp ? setGrossAmount(temp) : setGrossAmount();
                        }
                      }}
                    />
                  </Form.Group>
                </Col>
              </Row>
            )}

            {[
              "water_house",
              "light_house",
              "gas_house",
              "power_source_vehicle",
            ].includes(subgroup) && (
              <Row>
                <Col md={12}>
                  <Form.Group
                    label={
                      texts.consumptionSelect[language] +
                      " (" +
                      consumptionUd +
                      ")"
                    }
                    className="field-amount"
                    //style={{ paddingRight: "10px" }}
                  >
                    <Form.Input
                      name="consumption"
                      className="input"
                      placeholder="0,00"
                      autoComplete="off"
                      type="text"
                      value={
                        consumption != undefined
                          ? consumption.toString().replace(".", ",")
                          : ""
                      }
                      onChange={(e) => {
                        var temp = e.target.value;
                        if (/^[0-9]{0,9}([\.\,]([0-9]{1,2})?)?$/i.test(temp))
                          temp ? setConsumption(temp) : setConsumption();
                      }}
                    />
                  </Form.Group>
                </Col>
              </Row>
            )}

            {["power_source_vehicle", "maintenance_vehicle"].includes(
              subgroup
            ) && (
              <Row>
                <Col md={12}>
                  <Form.Group
                    label={texts.milometerSelect[language]}
                    className="field-amount"
                    //style={{ paddingRight: "10px" }}
                  >
                    <Form.Input
                      name="milometer"
                      className="input"
                      autoComplete="off"
                      placeholder="0"
                      type="text"
                      value={milometer != undefined ? milometer.toString() : ""}
                      onChange={(e) => {
                        var temp = e.target.value;
                        if (/^[0-9]{0,9}$/i.test(temp))
                          temp ? setMilometer(temp) : setMilometer();
                      }}
                      required
                    />
                  </Form.Group>
                </Col>
              </Row>
            )}

            <Row>
              <Col md={12}>
                <Form.Group label={texts.noteTextArea[language]}>
                  <Form.Textarea
                    clasName="field-note"
                    name="note"
                    placeholder="Nota informativa..."
                    value={note ? note : " "}
                    onChange={(e) => {
                      setNote(e.target.value);
                    }}
                    autoComplete="off"
                    // rows={5}
                  />
                </Form.Group>
              </Col>
            </Row>
            {type == "transfer" && (
              <Row>
                <Col md={12}>
                  <Form.Group label="Transferencia de:">
                    <Form.SwitchStack>
                      <Form.Switch
                        label="Acciones"
                        name="toggle"
                        type="radio"
                        value="stock"
                        checked={transferType == "stock"}
                        onChange={(e) => setTransferType(e.target.value)}
                      />
                      <Form.Switch
                        label="Otros"
                        name="toggle"
                        type="radio"
                        value={undefined}
                        checked={!transferType}
                        onChange={(e) => setTransferType(e.target.value)}
                      />
                    </Form.SwitchStack>
                  </Form.Group>
                </Col>
              </Row>
            )}
          </Col>
        </Row>
        <Row style={{ margin: "0px" }}>
          <Col md={8} style={{ height: "10px" }}></Col>
          <Col md={4} style={{ backgroundColor: "#eff0f2" }}></Col>
        </Row>
        {newRecord &&
        JSON.stringify(initialRecord) != JSON.stringify(newRecord) ? (
          <Row className="form-buttons" style={{ margin: "0px" }}>
            <Col md={8} style={{ padding: "0px", height: "60px" }}>
              <Row className="form-buttons" style={{ padding: "inherit" }}>
                <Col xs={6}>
                  <div style={{ float: "right" }}>
                    <Button
                      pill
                      outline
                      color="info"
                      onClick={() => setFields(fetchedRecord)}
                    >
                      Deshacer
                    </Button>
                  </div>
                </Col>
                <Col xs={6}>
                  {validForm && (
                    <div style={{ float: "left" }}>
                      <Button pill color="success" onClick={() => handleSave()}>
                        Guardar
                      </Button>
                    </div>
                  )}
                </Col>
              </Row>
            </Col>
            <Col md={4} style={{ backgroundColor: "#eff0f2" }}></Col>
          </Row>
        ) : (
          <Row style={{ height: "60px", margin: "0" }}>
            <Col md={8} style={{ padding: "0px", height: "60px" }}></Col>
            <Col md={4} style={{ backgroundColor: "#eff0f2" }}></Col>
          </Row>
        )}
      </>
      {/* )} */}
    </>
  );
};

export default AccountRecordForm;
