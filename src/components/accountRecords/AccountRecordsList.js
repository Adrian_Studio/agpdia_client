// @flow

import React from "react";
import { Redirect, useParams } from "react-router-dom";

import Infinite from "react-infinite";
import moment from "moment";
import ModalForm from "../ModalForm";
import { Container, Row, Col, Card, Button, ModalTitle } from "react-bootstrap";

//import { Card } from "tabler-react";
import fetchFunction from "../../helpers/fetchFuction";

import AccountRecord from "../accountRecords/AccountRecord";
import AccountRecordDate from "../accountRecords/AccountRecordDate";
import AccountRecordTotal from "../accountRecords/AccountRecordTotal";
import AccountRecordForm from "../accountRecords/AccountRecordForm";

import backButtonWithModal from "../../helpers/backButtonWithModal";

function AccountRecordsList({ props, updateSignal }) {
  const [error, setError] = React.useState();
  const [modalTitle, setModalTitle] = React.useState("");
  const [records, setRecords] = React.useState(false);
  const [recordsData, setRecordsData] = React.useState();
  const [concepts, setConcepts] = React.useState();
  const [conceptsInfo, setConceptsInfo] = React.useState();
  const [accounts, setAccounts] = React.useState();
  const [vehicles, setVehicles] = React.useState();
  const [houses, setHouses] = React.useState();
  const [selectedItems, setSelectedItem] = React.useState([]);
  const [itemsToForm, setItemsToForm] = React.useState([]);
  const [allRecords, setAllRecords] = React.useState([]);
  const [prevProps, setPrevProps] = React.useState({});
  const [firstRender, setFirstRender] = React.useState(false);
  const [modalType, setModalType] = React.useState();
  const [lastFetchAbort, setLastFetchAbort] = React.useState(); //Para guardar el abort del fetch actual

  var modalRef = React.useRef();

  React.useEffect(() => {
    // if (!firstRender) return;
    if (JSON.stringify(props) == JSON.stringify(prevProps)) return;
    if (props.hasDateFilter && !props.filter.start_date && !props.filter.year)
      return;
    setPrevProps(props);
  }, [props]);

  React.useEffect(() => {
    if (!firstRender) return;
    if (JSON.stringify(prevProps) == JSON.stringify({})) return;
    refetchRecords();
  }, [prevProps]);

  const updateSignalParent = () => {
    updateSignal();
  };
  const fetchRecords = () => {
    if (lastFetchAbort) {
      lastFetchAbort();
    }

    var start_date, end_date;
    if (props.filter.start_date) {
      start_date = props.filter.start_date;
      end_date = props.filter.end_date;
    } else if (props.filter.year && !props.filter.year.length) {
      start_date = moment(props.filter.year + "/01" + "/01").startOf("year");
      end_date = moment(props.filter.year + "/01" + "/01").endOf("year");
    } else if (props.filter.year && props.filter.year.length == 2) {
      start_date = moment(props.filter.year[0] + "/01" + "/01").startOf("year");
      end_date = moment(props.filter.year[1] + "/01" + "/01").endOf("year");
    }

    var filter = {
      start_date: start_date,
      end_date: end_date,
      concepts: props.filter.concepts,
      accounts: props.filter.accounts,
      type: props.filter.types,
      note: props.filter.note,
      group: props.filter.groups,
      subgroup: props.filter.subgroups,
      vehicles: props.filter.vehicles,
      houses: props.filter.houses,
    };

    setRecords(false);
    var [promise, abort] = fetchFunction(
      "accounts/aggs/records/list",
      "POST",
      filter
    );
    promise.then((data) => {
      if (data == undefined) return
      if (!data) {
        setRecords(
          <AccountRecordTotal
            key={new Date().getTime()}
            showModalHandler={showModalHandler}
          />
        );
      } else {
        setRecordsData(data);
        var allRecordsTemp = [];
        data.daily.forEach((day) => {
          day.records.forEach((record) => {
            allRecordsTemp.push(record.id);
          });
        });
        setAllRecords(allRecordsTemp);
        setSelectedItem([]);
      }
    });
    setLastFetchAbort(() => abort);
  };

  const fetchAccounts = () => {
    var [promise, abort] = fetchFunction("accounts/accounts/", "GET");

    if (promise.status == "404") {
      setError({
        type: "warning",
        message: "No se han encontrado cuentas para este usuario",
      });
    } else {
      promise.then((data) => {
        setAccounts(data);
      });
    }
  };

  const fetchVehicles = () => {
    var [promise, abort] = fetchFunction("vehicles/", "GET");
    if (promise.status == "404") {
      setError({
        type: "warning",
        message: "No se han encontrado vehículos para este usuario",
      });
    } else {
      promise.then((data) => {
        setVehicles(data);
      });
    }
  };

  const fetchHouses = () => {
    var [promise, abort] = fetchFunction("houses/", "GET");
    if (promise.status == "404") {
      setError({
        type: "warning",
        message: "No se han encontrado viviendas para este usuario",
      });
    } else {
      promise.then((data) => {
        setHouses(data);
      });
    }
  };

  const fetchConcepts = () => {
    var filter = {};
    if (props.toShow) {
      if (props.toShow.includes("house_concepts")) {
        filter.group = ["house"];
      }
      if (props.toShow.includes("vehicle_concepts")) {
        filter.group = ["vehicle"];
      }
      if (props.toShow.includes("stock_concepts")) {
        filter.group = ["stock"];
      }
      if (props.toShow.includes("invoice_concepts")) {
        filter.subgroup = ["water_house", "light_house", "gas_house"];
      }
      if (props.toShow.includes("paysheet_concepts")) {
        filter.subgroup = ["paysheet"];
      }
    }
    var [promise, abort] = fetchFunction(
      "accounts/aggs/concepts/filter_list",
      "POST",
      filter
    );

    if (promise.status == "404") {
      setError({
        type: "warning",
        message: "No se han encontrado cuentas para este usuario",
      });
    } else {
      promise.then((data) => {
        setConcepts(data);
      });
    }
  };

  const fetchConceptsInfo = () => {
    var [promise, abort] = fetchFunction("accounts/concepts", "GET");
    if (promise.status == "404") {
      setError({
        type: "warning",
        message: "No se han encontrado cuentas para este usuario",
      });
    } else {
      promise.then((data) => {
        setConceptsInfo(data);
      });
    }
  };

  const makeRecordsArray = () => {
    var recordsTable = [];

    recordsTable.push(
      <AccountRecordTotal
        key={new Date().getTime()}
        props={{
          amount: recordsData.total,
          selected: selectedItems,
          isChecked: selectedItems.length,
          toShow: props.toShow,
        }}
        updateSignal={updateSignalParent}
        totalCheckBoxHandler={totalCheckBoxHandler}
        showModalHandler={showModalHandler}
        refetchAfterDelete={refetchRecords}
      />
    );
    recordsData.daily.forEach((day) => {
      recordsTable.push(
        <AccountRecordDate
          key={new Date(day.id).getTime()}
          props={{
            date: day.date,
            amount: day.dailyTotal,
          }}
        />
      );

      day.records.forEach((record) => {
        selectedItems.includes(record.id)
          ? (record.isChecked = true)
          : (record.isChecked = false);

        recordsTable.push(
          <AccountRecord
            key={record.id}
            props={record}
            recordsCheckBoxHandler={recordsCheckBoxHandler}
            showModalHandler={showModalHandler}
          />
        );
      });
    });
    setRecords(recordsTable);
  };
  const dateHandler = (startDate, endDate) => {};
  const recordsFilterHandler = (concepts, accounts, type, note) => {};

  const recordsCheckBoxHandler = (objectId, isChecked, relatedObjectId) => {
    if (isChecked) {
      var temp = [...selectedItems]; // Need to clone the object to prevent malfunction
      temp.push(objectId);
      if (relatedObjectId) temp.push(relatedObjectId);
      setSelectedItem(temp);
    } else {
      var temp = [...selectedItems]; // Need to clone the object to prevent malfunction
      temp.splice(temp.indexOf(objectId), 1);
      if (relatedObjectId) temp.splice(temp.indexOf(relatedObjectId), 1);
      setSelectedItem(temp);
    }
  };

  const totalCheckBoxHandler = (isChecked) => {
    if (isChecked) {
      setSelectedItem(allRecords);
    } else {
      setSelectedItem([]);
    }
  };
  const showModalHandler = (objectId, toAdd) => {
    objectId ? setItemsToForm([objectId]) : setItemsToForm(selectedItems);
    if (toAdd) {
      if (selectedItems.length) {
        setModalTitle("Copiar registro");
        setModalType("Copy");
      } else {
        setItemsToForm([]);
        setModalTitle("Añadir registro");
        setModalType("Add");
      }
    } else if (objectId || selectedItems.length == 1) {
      setModalTitle("Editar registro");
      setModalType("UpdateOne");
    } else if (selectedItems.length) {
      setModalTitle(`Editar ${selectedItems.length} registros`);
      setModalType("UpdateMany");
    }
    backButtonWithModal.neutralizeBack(modalRef.current);
    modalRef.current.handleShow();
    modalRef.current.task(objectId, toAdd);
  };

  const modalSaveHandler = () => {
    modalRef.current.handleHide();

    if (updateSignal) {
      updateSignal();
    }
    refetchRecords();
  };

  const refetchRecords = () => {
    fetchRecords();
  };

  React.useEffect(() => {
    if (!prevProps) return;
    fetchAccounts();

    fetchHouses();

    fetchVehicles();

    fetchConcepts();

    fetchConceptsInfo();

    // if (props.filter.accounts) refetchRecords();
    setFirstRender(true);
  }, [prevProps]);

  React.useEffect(() => {
    if (!recordsData) return; //Dont create the array if there is no data
    if (!records && selectedItems.length) {
      setSelectedItem([]); //Thil will trigger again this useEffect
      return; //This cancel the creation of the array this time
    }

    makeRecordsArray();
  }, [recordsData, selectedItems]);

  return (
    <>
      {error && (
        <div className={"alert alert-" + error.type} role="alert">
          {error.message}
        </div>
      )}
      <Col lg={12} className="p-0">
        {!records ? (
          <h1>Loading...</h1>
        ) : (
          <Infinite
            preloadBatchSize={Infinite.containerHeightScaleFactor(3)}
            elementHeight={60} // The height of the tallest element in the list
            useWindowAsScrollContainer
          >
            {records}
          </Infinite>
        )}
      </Col>
      <ModalForm
        ref={modalRef}
        props={{
          title: modalTitle,
          className: "recordsForm",
        }}
      >
        <AccountRecordForm
          props={{
            objectId: itemsToForm,
            concepts: concepts,
            conceptsInfo: conceptsInfo,
            accounts: accounts,
            houses: houses,
            vehicles: vehicles,
            modalType: modalType,
            toShow: props.toShow,
          }}
          modalSaveHandler={modalSaveHandler}
        />
      </ModalForm>
    </>
  );
}

export default AccountRecordsList;
